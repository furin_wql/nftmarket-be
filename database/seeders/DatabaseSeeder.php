<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Bill;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Coupon;
use App\Models\NFT;
use App\Models\NotifySetting;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([UserSeeder::class]);
        Category::factory(3)->create();
        Collection::factory(5)->create();
        NFT::factory(20)->hasNftPrices(2)->create();
        Coupon::factory(10)->create();
        Cart::factory(20)->create();
        Bill::factory(10)->hasBillNFT(3)->create();
        NotifySetting::factory(5)->create();
        $this->call([NFTsCurrentPriceSeeder::class]);
    }
}
