<?php

namespace Database\Seeders;

use App\Models\NFT;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class NFTsCurrentPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $nfts= NFT::query();
        $nfts= NFT::with(['nftPrices'=> function ($q){
            $q->orderBy('created_at','asc');
        }]);
        $nfts = $nfts->with(['nftPrices'=> function ($q){
            $q->orderBy('created_at','asc');
        }]);
        $nfts = $nfts->get();
        foreach($nfts as $item) {
            $price = $item->getRelations('nftPrices')['nftPrices']->last()->getAttribute('price');
            $item['current_price'] = $price;
            $item->save();
        }
    }
}
