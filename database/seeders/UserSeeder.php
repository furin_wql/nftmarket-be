<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            User::create([
                'name' => 'admin',
                'avatar' => fake()->imageUrl(),
                'email' => 'admin@gmail.com',
                'gender' => fake()->randomElement(array ('male','female')),  
                'phone_number' => fake()->e164PhoneNumber(),
                'custom_url' => 'link/'.fake()->firstNameMale(),
                'street_address' => fake()->streetAddress(),
                'country_region' => fake()->country(),
                'state' => fake()->state(),
                'town_city' => fake()->city(),
                'zip_code' => fake()->numberBetween(100, 999),
                'introduce_yourseft' => fake()->text(40),
                'email_verified_at' => now(),
                'wallet_eth' => 99999.99,
                'password' => bcrypt('12345'),
                'remember_token' => Str::random(10),
            ]);
            User::factory(10)->create();
        } catch (\Throwable $th) {
        }
        
    }
}
