<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\NFT;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\NFT>
 */
class NFTFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = NFT::class;
    public function definition(): array
    {
        return [
            'title' =>"Art ".fake()->sentence(3),
            'user_id' =>  User::all()->random()->id,
            'collection_id'=> Category::all()->random()->id,
            'starting_date' =>  now(),
            'expiration_date' => now(),
            'description' => fake()->text(20),
            'img_url' => fake()->imageUrl(),
        ];
    }
}
