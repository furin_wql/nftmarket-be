<?php

namespace Database\Factories;

use App\Models\NotifySetting;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\NotifySetting>
 */
class NotifySettingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = NotifySetting::class;
    public function definition(): array
    {
        return [
            "name" => fake()->sentence(1),
        ];
    }
}
