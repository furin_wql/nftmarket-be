<?php

namespace Database\Factories;

use App\Models\Coupon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Coupon>
 */
class CouponFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Coupon::class;
    public function definition(): array
    {
        return [
            'name' => "Coupon " . fake()->sentence(1),
            'code' => fake()->unique()->lexify('cp????'),
            'percentage' => fake()->randomFloat(0, 1, 30),
            'quantity' => fake()->numberBetween(10,100),
            'starting_date' => now(),
            'expiration_date' => now()->addYears(1),
        ];
    }
}
