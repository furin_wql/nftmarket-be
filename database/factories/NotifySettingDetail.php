<?php

namespace Database\Factories;

use App\Models\NotifySetting;
use App\Models\NotifySettingDetail;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\NotifySettingDetail>
 */
class NotifyUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = NotifySettingDetail::class;
    public function definition(): array
    {
        return [
            "user_id" => User::all()->random()->id,
            "notify_setting_id" => NotifySetting::all()->random()->id,
        ];
    }
}
