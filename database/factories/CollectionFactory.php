<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Collection;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Collection>
 */
class CollectionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    protected $model = Collection::class;
    public function definition(): array
    {
        return [
            "user_id" => User::all()->random()->id,
            "category_id" => Category::all()->random()->id,
            "logo_img_url" => fake()->imageUrl(),
            "feature_img_url" => fake()->imageUrl(),
            "cover_img_url" => fake()->imageUrl(),
            "name" => "Collection " . fake()->sentence(1),
            "url" => "",
            "price" => fake()->randomFloat(2, 0, 100),
            "starting_date" => now(),
            "expiration_date" => now()->addYears(1),
            "description" => fake()->text(20),
            "is_explicit_and_sensitive" => fake()->boolean(50),
        ];
    }
}
