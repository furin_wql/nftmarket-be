<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * The current password being used by the factory.
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'avatar' => fake()->imageUrl(),
            'email' => fake()->unique()->safeEmail(),
            'gender' => fake()->randomElement(array ('male','female')),  
            'phone_number' => fake()->e164PhoneNumber(),
            'custom_url' => 'link/'.fake()->firstNameMale(),
            'street_address' => fake()->streetAddress(),
            'country_region' => fake()->country(),
            'state' => fake()->state(),
            'town_city' => fake()->city(),
            'zip_code' => fake()->numberBetween(100, 999),
            'introduce_yourseft' => fake()->text(40),
            'email_verified_at' => now(),
            'wallet_eth' => fake()->randomFloat(2,20,3000),
            'password' => bcrypt('12345'),
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
