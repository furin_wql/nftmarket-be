<?php

namespace Database\Factories;

use App\Models\Bill;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Bill>
 */
class BillFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Bill::class;
    public function definition(): array
    {
        return [
            'user_id' => User::all()->random()->id,
            'order_date' => now(),
            'total_prices' => fake()->numberBetween(200,800),
            'firstname' => fake()->name(),
            'lastname' => fake()->lastName(),
            'company_name' =>fake()->company(),
            'country_region' =>fake()->country(),
            'street_address' =>fake()->address(),
            'town_city' => fake()->city(),
            'state' =>fake()->state(),
            'zip_code' => fake()->numberBetween(100,999),
            'phone' => fake()->phoneNumber(),
            'email' =>fake()->email(),
        ];
    }
}
