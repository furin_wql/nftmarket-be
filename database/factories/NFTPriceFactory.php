<?php

namespace Database\Factories;

use App\Models\NFTPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\NFTPrice>
 */
class NFTPriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = NFTPrice::class;
    public function definition(): array
    {
        return [
            'price' => fake()->randomFloat(2, 0.1, 100),
        ];
    }
}
