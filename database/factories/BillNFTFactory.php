<?php

namespace Database\Factories;

use App\Models\BillNFT;
use App\Models\NFT;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Billdetail>
 */
class BillNFTFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = BillNFT::class;
    public function definition(): array
    {
        return [
            'nft_id' => NFT::all()->random()->id,
            'quantity' => fake()->numberBetween(1, 10),
            'price_per_unit' => fake()->numberBetween(200,800),
        ];
    }
}
