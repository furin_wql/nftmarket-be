<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->string('email')->unique();
            $table->string('gender')->default('male');
            $table->string('phone_number')->nullable();
            $table->string('custom_url')->nullable();
            $table->string('street_address')->nullable();
            $table->string('country_region')->nullable();
            $table->string('state')->nullable();
            $table->string('town_city')->nullable();
            $table->string('zip_code')->nullable();
            $table->text('introduce_yourseft')->nullable();
            $table->float('wallet_eth')->default(0);
            $table->string('confirm_google')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('verifyToken')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
