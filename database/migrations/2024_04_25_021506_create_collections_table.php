<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category_id');    
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedFloat('price');
            $table->string('name');
            $table->string('url');
            $table->dateTime('starting_date');
            $table->dateTime('expiration_date');
            $table->text('description')->nullable();
            $table->boolean('is_explicit_and_sensitive')->default(false);
            $table->string('logo_img_url')->nullable();
            $table->string('feature_img_url')->nullable();
            $table->string('cover_img_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('collections');
    }
};
