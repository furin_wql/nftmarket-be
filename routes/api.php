<?php

use App\Http\Controllers\User\ConfirmGoogleCallback;
use App\Http\Controllers\Auth\ForgotPasword;
use App\Http\Controllers\User\GetURLConfirmGoogle;
use App\Http\Controllers\Favorite\AddFavoriteCollection;
use App\Http\Controllers\Favorite\AddFavoriteNFT;
use App\Http\Controllers\Favorite\GetFavoriteNFT;
use App\Http\Controllers\User\GetListUser;
use App\Http\Controllers\User\GetTopSellers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginUser;
use App\Http\Controllers\Auth\LogoutUser;
use App\Http\Controllers\Auth\ResetPassword;
use App\Http\Controllers\Auth\RuntestController;
use App\Http\Controllers\Auth\Signup;
use App\Http\Controllers\Auth\VerifyEmail;
use App\Http\Controllers\Cart\DecreaseCartItem;
use App\Http\Controllers\Collection\CreateCollection;
use App\Http\Controllers\Collection\GetCollectionDetail;
use App\Http\Controllers\Collection\GetListCollections;
use App\Http\Controllers\NFT\AddNFTtoCart;
use App\Http\Controllers\NFT\CreateNFT;
use App\Http\Controllers\NFT\CreateNFTPrice;
use App\Http\Controllers\Cart\DeleteCart;
use App\Http\Controllers\Cart\GetCart;
use App\Http\Controllers\Cart\IncreaseCartItem;
use App\Http\Controllers\Checkouts\Checkout;
use App\Http\Controllers\Checkouts\GetOrder;
use App\Http\Controllers\Collection\GetTopCollections;
use App\Http\Controllers\Contacts\ContactMail;
use App\Http\Controllers\Favorite\GetFavoriteCollection;
use App\Http\Controllers\NFT\GetNFTDetail;
use App\Http\Controllers\NFT\SearchNFTs;
use App\Http\Controllers\Notif\GetNotifSetting;
use App\Http\Controllers\Notif\ToggleSetting;
use App\Http\Controllers\User\ChangePassword;
use App\Http\Controllers\User\GetProfile;
use App\Http\Controllers\User\UpdateProfile;
use App\Http\Controllers\User\UpdateProfileAvatar;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::get("/runtest", RuntestController::class);

Route::post("/login", LoginUser::class)->middleware(["checksession.api"]);
Route::get("/logout", LogoutUser::class);
Route::post('/signup',Signup::class);
Route::post('/forgot-password', ForgotPasword::class);
Route::post('/reset-password', ResetPassword::class);
Route::get('/nfts/{nft_id}',GetNFTDetail::class);
Route::get('/nfts',SearchNFTs::class);
Route::get("users/list", GetListUser::class);
Route::get('/email/verify/{id}/{hash}',VerifyEmail::class);
Route::get('/top-collection', GetTopCollections::class);
Route::get('/top-sellers',GetTopSellers::class);
Route::get("/collections", GetListCollections::class);

Route::middleware("auth")->group(function () {
    Route::post("/nfts", CreateNFT::class);
    Route::group(["prefix" => "users"], function () {
        Route::get("", GetProfile::class);
        Route::get("/notify-setting", GetNotifSetting::class);
        Route::post('/notify-setting',ToggleSetting::class);
        Route::put("", UpdateProfile::class);
        Route::put("/avatar", UpdateProfileAvatar::class);
        Route::put("/changepassword", ChangePassword::class);
        Route::post('/contact',ContactMail::class)->middleware('throttle:2,5');
        Route::get("/confirm/google", GetURLConfirmGoogle::class);
        Route::get("/confirm/google/callback", ConfirmGoogleCallback::class);
    });
    Route::group(["prefix" => "collections"], function () {
        Route::post("", CreateCollection::class);
        Route::get('{collection_id}',GetCollectionDetail::class);
    });
    Route::group(["prefix" => "favorite"], function () {
        Route::get("collections", GetFavoriteCollection::class);
        Route::post("collections", AddFavoriteCollection::class);
        Route::post('nfts',AddFavoriteNFT::class);
        Route::get('nfts',GetFavoriteNFT::class);
    });
    Route::post('/nftprice',CreateNFTPrice::class);

    Route::group(["prefix" => "carts"], function () {
        Route::post('',AddNFTtoCart::class);
        Route::get('',GetCart::class);
        Route::delete('{cart_id}',DeleteCart::class);
        Route::put('{cart_id}/increase',IncreaseCartItem::class);
        Route::put('{cart_id}/decrease',DecreaseCartItem::class);
    });
    Route::post('/checkout',Checkout::class);
    Route::post('/orders',GetOrder::class);
});



