<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bill extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'coupon_id',
        'order_date',
        'total_prices',
        'firstname',
        'lastname',
        'company_name',
        'country_region',
        'street_address',
        'town_city',
        'state',
        'zip_code',
        'phone',
        'email',
        'note',
    ];
    public function billNFT(): HasMany
    {
        return $this->hasMany(BillNFT::class, 'bill_id');
    }
}
