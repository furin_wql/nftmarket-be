<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NFTPrice extends Model
{
    use HasFactory;
    protected $table = 'nft_prices';
    protected $fillable = [
        'nft_id',
        'price',
    ];
    public function NFT() : BelongsTo
    {
        return $this->belongsTo(NFT::class);
    }
}
