<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BillNFT extends Model
{
    use HasFactory;
    protected $table = 'bill_nft';
    protected $fillable = [
        'bill_id',
        'nft_id',
        'quantity',
        'price_per_unit',
    ];

    public function nfts(): BelongsTo
    {
        return $this->belongsTo(NFT::class, 'nft_id');
    }
}
