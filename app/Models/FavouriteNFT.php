<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouriteNFT extends Model
{
    use HasFactory;
    protected $table ="favourite_nft";
    protected $fillable = [
        "nft_id",
        "user_id",
    ];
}
