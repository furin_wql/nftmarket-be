<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class NFT extends Model
{
    use HasFactory;
    protected $table = 'nfts';
    protected $fillable = [
        'title',
        'user_id',
        'collection_id',
        'starting_date',
        'expiration_date',
        'description',
        'img_url',
        'current_price',
    ];
    public function nftPrices(): HasMany
    {
        return $this->hasMany(NFTPrice::class, 'nft_id');
    }
    public function carts()
    {
        return $this->belongsTo(NFT::class);
    }
}
