<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotifySettingDetail extends Model
{
    use HasFactory;
    protected $table = 'notify_setting_details';
    protected $fillable = [
        'user_id',
        'notify_setting_id',
        'value'
    ];
}
