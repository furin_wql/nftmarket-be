<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'user_id',
        'name',
        'url',
        'price',
        'starting_date',
        'expiration_date',
        'description',
        'is_explicit_and_sensitive',
        'logo_img_url',
        'feature_img_url',
        'cover_img_url',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function nft()
    {
        return $this->hasMany(NFT::class);
    }
}
