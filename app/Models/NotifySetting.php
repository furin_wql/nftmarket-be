<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotifySetting extends Model
{
    use HasFactory;
    protected $table = 'notify_settings';
    protected $fillable = [
        'name',
    ];

}
