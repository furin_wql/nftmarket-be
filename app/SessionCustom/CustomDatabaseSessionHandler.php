<?php

namespace App\SessionCustom;


use Illuminate\Session\DatabaseSessionHandler;
use App\Models\SessionModel;

class CustomDatabaseSessionHandler extends DatabaseSessionHandler{
    public function write($sessionId, $data): bool
    {
        $payload = $this->getDefaultPayload($data);
        if (! $this->exists) {
            $this->read($sessionId);
        }
        if(!$this->userId()){
            $sessionRefresh = $this->container->make('request')->header('cookie');
            session()->put('sss',$sessionRefresh);
            $session = SessionModel::with('user')->where('id',$sessionRefresh)->first();
            if($session){
                $payload['user_id'] = $session->user->id;
                $this->performUpdate($sessionRefresh,$payload);
                return $this->exists = true;
            }
            return $this->exists = false;
        }else{

            if ($this->exists) {
                $this->performUpdate($sessionId, $payload);
            } else {
                $this->performInsert($sessionId, $payload);
            }

        }
        $this->gc(config('session.lifetime')*60);
        return $this->exists = true;
    }
}