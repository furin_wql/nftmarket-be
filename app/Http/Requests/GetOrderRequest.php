<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'cart_ids' => [
                'required',
                'array',
                'exists' => Rule::exists('cart','id')->where('user_id',$this->user()->id),
            ],
        ];
    }
    public function messages(){
        return [
           'cart_ids.array' => 'cart_ids must be an array containing cart ids',
           'exists' => 'appears id in the list is not owned',
        ];
    }
}
