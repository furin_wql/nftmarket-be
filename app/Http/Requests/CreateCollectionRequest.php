<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "logo_img" => ["nullable", "image", "max:5120"],
            "featured_img" => ["nullable", "image", "max:5120"],
            "cover_img" => ["nullable", "image", "max:5120"],
            "name" => "required",
            "url" => "required",
            "price" => "required|numeric",
            "category_id" => "required",
            "starting_date" => "required|date",
            "expiration_date" => "required|date",
            "description" => "nullable",
            "is_explicit_and_sensitive" => "required|boolean",];
    }
}
