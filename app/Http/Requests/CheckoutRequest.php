<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'cart_ids' => [
                'required',
                'array',
                'exists' => Rule::exists('cart','id')->where('user_id',$this->user()->id),
            ],
            'firstname'=>'required',
            'lastname'=>'required',
            'company_name' => 'string|max:255|nullable',
            'country_region'=>'required',
            'street_address'=>'required',
            'town_city'=>'required',
            'zip_code'=>'required',
            'phone'=>'required|numeric',
            'email'=>'required',
            'state'=>'required',
            'note'=> 'string|max:255|nullable',

        
        ];
    }
    public function messages()
    {
        return [
            'cart_ids.array' => 'cart_ids must be an array containing cart ids',
            'cart_ids.exists' => 'appears id in the list is not owned',
        ];
    }
}
