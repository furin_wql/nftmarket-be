<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Auth;
use App\Helpers\ResponseHelper;
use Carbon\Carbon;

use App\Models\SessionModel;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){
            return ResponseHelper::success('User is already logged in',null,ResponseHelper::HTTP_UNAUTHORIZED);
        }
        return $next($request);
    }
}
