<?php

namespace App\Http\Controllers\Collection;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Collection;
use Illuminate\Http\Request;

class GetListCollections extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/collections",
     *     summary="Get list of collections",
     *     operationId="GetListCollections",
     *     tags={"Collection"},
     *     @OA\Parameter(
     *         name="category_id",
     *         in="path",
     *         required=false,
     *         description="ID category of the collection",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="search",
     *         in="path",
     *         required=false,
     *         description="Search collection by name",
     *         @OA\Schema(
     *             type="string",
     *         )
     *     ),
     *  @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         type="object",
     *    @OA\Property(
     *        property="success",
     *        type="boolean",
     *    ),
     *    @OA\Property(
     *        property="message",
     *        type="string",
     *        example="get list collections successfully",
     *    ),
     *    @OA\Property(
     *        property="data",
     *        type="object",
     *        @OA\Property(
     *            property="current_page",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="data",
     *            type="array",
     *            @OA\Items(
     *                type="object",
     *                @OA\Property(
     *                    property="id",
     *                    type="number",
     *                ),
     *                @OA\Property(
     *                    property="created_at",
     *                    type="string",
     *                    example="2024-05-06T04:46:54.000000Z",
     *                ),
     *                @OA\Property(
     *                    property="updated_at",
     *                    type="string",
     *                    example="2024-05-06T04:46:54.000000Z",
     *                ),
     *                @OA\Property(
     *                    property="user_id",
     *                    type="number",
     *                ),
     *                @OA\Property(
     *                    property="category_id",
     *                    type="number",
     *                ),
     *                @OA\Property(
     *                    property="price",
     *                    type="string",
     *                    example="17.57",
     *                ),
     *                @OA\Property(
     *                    property="name",
     *                    type="string",
     *                    example="Collection Sed.",
     *                ),
     *                @OA\Property(
     *                    property="url",
     *                    type="string",
     *                ),
     *                @OA\Property(
     *                    property="starting_date",
     *                    type="string",
     *                    example="2024-05-06 04:46:54",
     *                ),
     *                @OA\Property(
     *                    property="expiration_date",
     *                    type="string",
     *                    example="2025-05-06 04:46:54",
     *                ),
     *                @OA\Property(
     *                    property="description",
     *                    type="string",
     *                    example="Minima laboriosam.",
     *                ),
     *                @OA\Property(
     *                    property="is_explicit_and_sensitive",
     *                    type="boolean",
     *                ),
     *                @OA\Property(
     *                    property="logo_img_url",
     *                    type="string",
     *                    example="https://via.placeholder.com/640x480.png/0011bb?text=itaque",
     *                ),
     *                @OA\Property(
     *                    property="feature_img_url",
     *                    type="string",
     *                    example="https://via.placeholder.com/640x480.png/00bb33?text=suscipit",
     *                ),
     *                @OA\Property(
     *                    property="cover_img_url",
     *                    type="string",
     *                    example="https://via.placeholder.com/640x480.png/00ffaa?text=dolorum",
     *                ),
     *            ),
     *        ),
     *        @OA\Property(
     *            property="first_page_url",
     *            type="string",
     *            example="http://localhost/api/collections?page=1",
     *        ),
     *        @OA\Property(
     *            property="from",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="last_page",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="last_page_url",
     *            type="string",
     *            example="http://localhost/api/collections?page=1",
     *        ),
     *        @OA\Property(
     *            property="links",
     *            type="array",
     *            @OA\Items(
     *                type="object",
     *                @OA\Property(
     *                    property="url",
     *                    format="nullable",
     *                    type="string",
     *                ),
     *                @OA\Property(
     *                    property="label",
     *                    type="string",
     *                    example="&laquo; Previous",
     *                ),
     *                @OA\Property(
     *                    property="active",
     *                    type="boolean",
     *                ),
     *            ),
     *        ),
     *        @OA\Property(
     *            property="next_page_url",
     *            format="nullable",
     *            type="string",
     *        ),
     *        @OA\Property(
     *            property="path",
     *            type="string",
     *            example="http://localhost/api/collections",
     *        ),
     *        @OA\Property(
     *            property="per_page",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="prev_page_url",
     *            format="nullable",
     *            type="string",
     *        ),
     *        @OA\Property(
     *            property="to",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="total",
     *            type="number",
     *        ),
     *    ),
     *         ),
     *     ),
     * ),
     *     @OA\Response(
     *         response=500,
     *         description="An error occurred while get list Collections"
     *     )
     * )
     */

    public function __invoke(Request $request)
    {
        $collections = Collection::query();

        if ($request["category_id"]) {
            $collections = $collections = $collections->where("category_id",$request->category_id);
        }

        if ($request["user_id"]) {
            $collections = $collections = $collections->where("user_id",$request->user_id);
        }

        if ($request["search"]) {
            $collections = $collections = $collections->where(
                "name",
                "ilike",
                "%" . $request->search . "%"
            );
        }

        $collections = $collections->paginate(10);

        return ResponseHelper::success("Get list collections successfully",$collections);
    }
}
