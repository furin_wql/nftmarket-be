<?php

namespace App\Http\Controllers\Collection;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCollectionRequest;
use App\Models\Collection;
use AWS\CRT\HTTP\Response;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CreateCollection extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/collections",
     *     summary="Create a Collection",
     *     operationId="createCollection",
     *     tags={"Collection"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *     @OA\Property(
     *        property="name",
     *        type="string",
     *        example="ewf",
     *    ),
     *    @OA\Property(
     *        property="url",
     *        type="string",
     *        example="google.com",
     *    ),
     *    @OA\Property(
     *        property="price",
     *        type="number",
     *    ),
     *    @OA\Property(
     *        property="category_id",
     *        type="number",
     *    ),
     *    @OA\Property(
     *        property="starting_date",
     *        type="string",
     *        example="2024-05-02 07:15:13",
     *    ),
     *    @OA\Property(
     *        property="expiration_date",
     *        type="string",
     *        example="2025-05-02 07:15:13",
     *    ),
     *    @OA\Property(
     *        property="description",
     *        type="string",
     *        example="wef",
     *    ),
     *    @OA\Property(
     *        property="is_explicit_and_sensitive",
     *        type="number",
     *    ),
     *    @OA\Property(
     *        property="logo_img_url",
     *        type="string",
     *        format="binary",
     *        description="logo of the NFT"
     *     ),
     *     @OA\Property(
     *        property="feature_img_url",
     *        type="string",
     *        format="binary",
     *        description="featured image of the NFT"
     *     ),
     *    @OA\Property(
     *        property="cover_img_url",
     *        type="string",
     *        format="binary",
     *        description="cover image of the NFT"
     *     ),
     * ),
     * ),
     * ),
     * @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         type="object",
     *    @OA\Property(
     *        property="success",
     *        type="boolean",
     *    ),
     *    @OA\Property(
     *        property="message",
     *        type="string",
     *        example="Collection created successfully",
     *    ),
     *    @OA\Property(
     *        property="data",
     *        type="object",
     *        @OA\Property(
     *            property="name",
     *            type="string",
     *            example="ewf",
     *        ),
     *        @OA\Property(
     *            property="url",
     *            type="string",
     *            example="google.com",
     *        ),
     *        @OA\Property(
     *            property="price",
     *            type="string",
     *            example="123123",
     *        ),
     *        @OA\Property(
     *            property="category_id",
     *            type="string",
     *            example="1",
     *        ),
     *        @OA\Property(
     *            property="starting_date",
     *            type="string",
     *            example="2024-05-02 07:15:13",
     *        ),
     *        @OA\Property(
     *            property="expiration_date",
     *            type="string",
     *            example="2025-05-02 07:15:13",
     *        ),
     *        @OA\Property(
     *            property="description",
     *            type="string",
     *            example="wef",
     *        ),
     *        @OA\Property(
     *            property="is_explicit_and_sensitive",
     *            type="string",
     *            example="1",
     *        ),
     *        @OA\Property(
     *            property="logo_img_url",
     *            type="string",
     *            example="NFTFiles/ecGxiyjA920SA9HqOpQUrOvzd8zMtwMQET65UvLc.jpg",
     *        ),
     *        @OA\Property(
     *            property="feature_img_url",
     *            type="string",
     *            example="NFTFiles/jGOOGRc6CGeOhFLTjfA3o6JW6tVHy2PjLYxW6VOy.jpg",
     *        ),
     *        @OA\Property(
     *            property="cover_img_url",
     *            type="string",
     *            example="NFTFiles/I0St4NqTCrXcwKHuGn2Z0oXagerZZXgI2mPIDtqL.jpg",
     *        ),
     *        @OA\Property(
     *            property="user_id",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="updated_at",
     *            type="string",
     *            example="2024-05-06T06:53:50.000000Z",
     *        ),
     *        @OA\Property(
     *            property="created_at",
     *            type="string",
     *            example="2024-05-06T06:53:50.000000Z",
     *        ),
     *        @OA\Property(
     *            property="id",
     *            type="number",
     *        ),
     *    ),
     * ),
     * ),
     *     @OA\Response(
     *         response=500,
     *         description="An error occurred while create list Collections"
     *     )
     * )
     *
     */
    public function __invoke(CreateCollectionRequest $request)
    {
        try {
            $collection = $request->validated();
            $collection["logo_img_url"] = $this->uploadImage(
                $request,
                "logo_img"
            );
            $collection["feature_img_url"] = $this->uploadImage(
                $request,
                "featured_img"
            );
            $collection["cover_img_url"] = $this->uploadImage(
                $request,
                "cover_img"
            );

            $collection["user_id"] = Auth::id();
            $collection = Collection::create($collection);
            return ResponseHelper::success(
                message:"Collection created successfully",
                statusCode:ResponseHelper::HTTP_CREATED
            );  
        } catch (Exception $e) {
            return ResponseHelper::error($e->getMessage());
        }
    }

    private function uploadImage(CreateCollectionRequest $request, $upload_file)
    {
        if ($request->file($upload_file)) {
            $image = $request->file($upload_file);
            return Storage::disk("minio")->put("NFTFiles", $image);
        }
        return null;
    }
}
