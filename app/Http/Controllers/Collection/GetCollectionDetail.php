<?php

namespace App\Http\Controllers\Collection;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Collection;
use Illuminate\Http\Request;

class GetCollectionDetail extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/collections/{id}",
     *     summary="Get details of a Collection",
     *     operationId="CollectionId",
     *     tags={"Collection"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the Collection",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     * @OA\Response(
     *     response=200,
     *     description="Success",
     *     @OA\JsonContent(
     *         type="object",
     *    @OA\Property(
     *        property="success",
     *        type="boolean",
     *    ),
     *    @OA\Property(
     *        property="message",
     *        type="string",
     *        example="get collection detail successfully",
     *    ),
     *    @OA\Property(
     *        property="data",
     *        type="object",
     *        @OA\Property(
     *            property="id",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="created_at",
     *            type="string",
     *            example="2024-05-06T04:46:54.000000Z",
     *        ),
     *        @OA\Property(
     *            property="updated_at",
     *            type="string",
     *            example="2024-05-06T04:46:54.000000Z",
     *        ),
     *        @OA\Property(
     *            property="user_id",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="category_id",
     *            type="number",
     *        ),
     *        @OA\Property(
     *            property="price",
     *            type="string",
     *            example="40.73",
     *        ),
     *        @OA\Property(
     *            property="name",
     *            type="string",
     *            example="Collection Similique.",
     *        ),
     *        @OA\Property(
     *            property="url",
     *            type="string",
     *        ),
     *        @OA\Property(
     *            property="starting_date",
     *            type="string",
     *            example="2024-05-06 04:46:54",
     *        ),
     *        @OA\Property(
     *            property="expiration_date",
     *            type="string",
     *            example="2025-05-06 04:46:54",
     *        ),
     *        @OA\Property(
     *            property="description",
     *            type="string",
     *            example="At repellendus sed.",
     *        ),
     *        @OA\Property(
     *            property="is_explicit_and_sensitive",
     *            type="boolean",
     *        ),
     *        @OA\Property(
     *            property="logo_img_url",
     *            type="string",
     *            example="https://via.placeholder.com/640x480.png/006644?text=facere",
     *        ),
     *        @OA\Property(
     *            property="feature_img_url",
     *            type="string",
     *            example="https://via.placeholder.com/640x480.png/00ccaa?text=in",
     *        ),
     *        @OA\Property(
     *            property="cover_img_url",
     *            type="string",
     *            example="https://via.placeholder.com/640x480.png/004433?text=aut",
     *        ),
     *        @OA\Property(
     *            property="catergory_name",
     *            type="string",
     *            example="Category Est.",
     *        ),
     *        @OA\Property(
     *            property="nfts_count",
     *            type="number",
     *        ),
     *    ),
     *         ),
     *     ),
     * ),
     *     @OA\Response(
     *         response=404,
     *         description="Collection not found",
     *     )
     * )
     */
    public function __invoke(Request $request, string $collection_id)
    {
        $collection = Collection::find($collection_id);
        if (!$collection) {
            return ResponseHelper::error(
                "collection not found",
                ResponseHelper::HTTP_NOT_FOUND
            );
        }
        $collection["catergory_name"] = $collection->category->name;
        $collection["nfts_count"] = $collection->nft->count();
        unset($collection["category"]);
        unset($collection["nft"]);
        return ResponseHelper::success(
            "get collection detail successfully",
            data: $collection
        );
    }
}
