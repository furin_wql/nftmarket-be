<?php

namespace App\Http\Controllers\Collection;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\BillNFT;
use App\Models\Collection;
use App\Models\NFT;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Get(
 *     path="/api/top-collection",
 *     summary="Get top collections",
 *     operationId="getTopCollections",
 *     tags={"Collection"},
 *     @OA\Response(
 *         response=200,
 *         description="Successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="successfully"),
 *             @OA\Property(
 *                 property="data",
 *                 type="object",
 *                 @OA\Property(
 *                     property="3",
 *                     type="object",
 *                     @OA\Property(property="collection_id", type="integer", example=3),
 *                     @OA\Property(property="volume", type="string", example="6478.8"),
 *                     @OA\Property(property="floor_price", type="string", example="40.3"),
 *                     @OA\Property(property="percent_change", type="number", example=0),
 *                     @OA\Property(property="sales", type="integer", example=2),
 * 
 *                     @OA\Property(property="percent_unique_owners", type="string", example="66.67"),
 *                     @OA\Property(property="percent_unique_owners_detail", type="string", example="6"),
 *                     @OA\Property(property="percent_items_listed", type="string", example="22.22"),
 *                     @OA\Property(property="percent_items_listed_detail", type="string", example="2 of 9"),
 *                 ),
 *                 @OA\Property(
 *                     property="1",
 *                     type="object",
 *                     @OA\Property(property="collection_id", type="integer", example=1),
 *                     @OA\Property(property="volume", type="string", example="868.8"),
 *                     @OA\Property(property="floor_price", type="string", example="70.81"),
 *                     @OA\Property(property="percent_change", type="string", example="778.64"),
 *                     @OA\Property(property="sales", type="integer", example=1),
 *                     
 *                     @OA\Property(property="percent_unique_owners", type="string", example="100.00"),
 *                     @OA\Property(property="percent_unique_owners_detail", type="string", example="6"),
 *                     @OA\Property(property="percent_items_listed", type="string", example="16.67"),
 *                     @OA\Property(property="percent_items_listed_detail", type="string", example="1 of 6"),
 * 
 *                 ),
 *             ),
 *         ),
 *     ),
 * )
 */


class GetTopCollections extends Controller
{

    public function __invoke()
    {
        $data = $this->calulatorPercent();
        return ResponseHelper::success(
            message:"successfully",
            data:$data,
            statusCode:ResponseHelper::HTTP_OK
        );
    }
    private function getTotalUsersCollection(){
        return NFT::select('nfts.collection_id', DB::raw('COUNT(DISTINCT nfts.user_id) as total_user'))
        ->groupBy('nfts.collection_id')
        ->get()
        ->keyBy('collection_id');
    }
    private function getTotalNFTsCollection(){
        return NFT::select('nfts.collection_id',DB::raw('COUNT(*) as total_price'))
        ->groupBy('nfts.collection_id')
        ->get()
        ->keyBy('collection_id');
    }
    private function getCollection(){
        return Collection::select('id','price')->get()
        ->keyBy('id');
    }
    private function calulatorPercent()
    {
        try{
            $currentWeek = $this->CycleSeventDay();
            $previousWeek = $this->previousWeek();
            $currenPrice = $this->getCollectionPrice($currentWeek['cycleStartDay'], $currentWeek['cycleEndDay']);
            $previousPrice = $this->getCollectionPrice($previousWeek['startDay_P_W'], $previousWeek['endDay_P_W']);
            $totalNftCollection = $this->getTotalNFTsCollection();
            $totalUserCollection = $this->getTotalUsersCollection();
            $collection = $this->getCollection();

            $collectionChanges = $currenPrice->map(function($curren) use ($previousPrice,$totalNftCollection,$totalUserCollection,$collection){
                // $previous = $previousPrice->firstWhere('collection_id', $curren->collection_id);
                $previousPrices = $previousPrice->get($curren->collection_id)->total_prices?? 0;
                $change = $previousPrices ? number_format((($curren->total_prices - $previousPrices)/$previousPrices)*100,2):null;

                $totalNft = $totalNftCollection->get($curren->collection_id)->total_price?? 0;
                $percentListed = $totalNft? number_format(($curren->sales/ $totalNft) *100 ,2):0;

                $totalUser = $totalUserCollection->get($curren->collection_id)->total_user?? 0;
                $percentOwner = $totalUser ? number_format(($totalUser/$totalNft)*100,2): 0;

                $floorPrice = $collection->get($curren->collection_id)->price??0;

                return [
                    'collection_id' => $curren->collection_id,
                    'volume' => $curren->total_prices,
                    'floor_price' => $floorPrice,
                    'percent_change' => $change == null? $change=0:$change,
                    'sales' => $curren->sales,
                    'percent_unique_owners' => $percentOwner,
                    'percent_unique_owners_detail' => $totalUser,
                    'percent_items_listed' => $percentListed,
                    'percent_items_listed_detail' => $curren->sales.' of '.$totalNft,

                ];
            });
            return $collectionChanges;
        }catch(Exception $e){
            return  ResponseHelper::error(
                message:$e->getMessage(),
            );
        }
    }

    private function getCollectionPrice($startDay, $endDay)
    {
        try{
            $collectionPrices = BillNFT::join('nfts','bill_nft.nft_id' ,'=', 'nfts.id')
            ->whereBetween('bill_nft.created_at',[$startDay,$endDay])
            ->select('nfts.collection_id', DB::raw('SUM(bill_nft.price_per_unit * bill_nft.quantity) as total_prices'),
                DB::raw('COUNT(bill_nft.nft_id) as sales'),
                )
            ->groupBy('nfts.collection_id')
            ->orderBy('total_prices','desc')
            ->get()
            ->keyBy("collection_id");
            return $collectionPrices;

        }catch(Exception $e){
            return ResponseHelper::error(
                message:$e->getMessage(),
            );
        }
    }

    private function CycleSeventDay(){

        $currentday = Carbon::now('Asia/Ho_Chi_Minh');
        $dateOfWeek = $currentday->dayOfWeek;
        if(!$dateOfWeek == Carbon::SUNDAY){
            $countaddstartday = $dateOfWeek == Carbon::MONDAY? 0 : $dateOfWeek - Carbon::MONDAY;
        }else{
            $countaddstartday = 6;
        } 
        $cycleStartDay = $currentday->copy()->subDays($countaddstartday);
        $cycleEndDay = $dateOfWeek == Carbon::SUNDAY ? $currentday->copy() :$cycleStartDay->copy()->addDays(6);

        return ['cycleStartDay' => $cycleStartDay , 'cycleEndDay' => $cycleEndDay];
    }

    private function previousWeek(){
        $cycleCurren = $this->CycleSeventDay();
        $previousWeek_date = $cycleCurren['cycleEndDay']->dayOfWeek == Carbon::SUNDAY
            ?$cycleCurren['cycleStartDay']->copy()->subWeek()->subDay() : $cycleCurren['cycleStartDay']->copy()->subWeek();
        $dateOfWeek = $previousWeek_date->dayOfWeek;
        $countaddstartday = $dateOfWeek == Carbon::MONDAY? 0 : $dateOfWeek - Carbon::MONDAY;
        $previousWeekStartDay = $previousWeek_date->copy()->subDays($countaddstartday);
        $previousWeekEndDay =  $previousWeekStartDay->copy()->addDays(6);

        return ['startDay_P_W' => $previousWeekStartDay , 'endDay_P_W' => $previousWeekEndDay];
    }
}
