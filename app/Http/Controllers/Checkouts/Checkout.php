<?php

namespace App\Http\Controllers\Checkouts;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use App\Models\Bill;
use App\Models\BillNFT;
use App\Models\Cart;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Post(
 *     path="/api/checkout",
 *     summary="checkout",
 *     operationId="checkout",
 *      security={{"cookieAuth": {}}},
 *     tags={"Checkouts"},
 * @OA\RequestBody(
 *         required=true,
 *         description="Array of cart IDs",
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 *                 type="object",
 *                 required={"cart_ids"},
 * @OA\Property(
 *                     property="cart_ids",
 *                     type="array",
 * @OA\Items(type="integer"),
 *                 ),
 *             ),
 *         ),
 *     ),
 * @OA\Response(
 *     response=422,
 *     description="The given data was invalid",
 * @OA\JsonContent(
 *         type="object",
 * @OA\Property(property="code",    type="integer", example=422),
 * @OA\Property(property="message", type="string", example="The given data was invalid"),
 * @OA\Property(
 *             property="errors",
 *             type="object",
 * @OA\Property(
 *                 property="cart_ids",
 *                 type="array",
 * @OA\Items(type="string",         example="appears id in the list is not owned")
 *             )
 *         )
 *     )
 * ),
 * @OA\Response(
 *         response=200,
 *         description="Checkout Successfully",
 * @OA\JsonContent(
 *             type="object",
 * @OA\Property(property="success", type="boolean", example=true),
 * @OA\Property(property="message", type="string", example="successfully"),
 * 
 *    ),
 *         )
 *     )
 * )
 */

class Checkout extends Controller
{
    public function __invoke(CheckoutRequest $request)
    {
        try{
            $cartIds= $request->input('cart_ids');
            $checkout = $request->validated();
            $user = Auth::user();
            $wallet_eth= $user->wallet_eth;

            $carts =Cart::with('nfts:id,title')->whereIn('id', $cartIds)->with('nfts')->get();

            $total_price = 0;
            foreach($carts as $cart){
                $total_price += $cart->quantity * $cart->nfts->current_price;
            };
            if(($wallet_eth < $total_price)){
                return ResponseHelper::error("You don't have enough money to pay");
            }  
            $checkout["user_id"] = Auth::id();
            $checkout["order_date"] = date("Y-m-d H:i:s");
            $checkout["total_prices"] =  $total_price;

            $bill = Bill::create($checkout);
            $billId = $bill->id;
            foreach($carts as $cart){
                BillNFT::create([
                    'bill_id' => $billId,
                    'nft_id' => $cart->nfts->id,
                    'quantity' => $cart->quantity,
                    'price_per_unit' => $cart->nfts->current_price,
                ]);
                $cart->delete();   
            };
            $user->wallet_eth -= $total_price;
            $user->save();

            return ResponseHelper::success(
                message:"Checkout success",
                statusCode:ResponseHelper::HTTP_CREATED
            );    
        }catch(Exception $e){
            return ResponseHelper::error($e->getMessage());
        }
        
    }
}
