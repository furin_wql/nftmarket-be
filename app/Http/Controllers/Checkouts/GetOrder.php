<?php

namespace App\Http\Controllers\Checkouts;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\GetOrderRequest;
use App\Models\Cart;
use App\Models\NFT;
use Exception;
use Illuminate\Http\Request;
use stdClass;

/**
 * @OA\Post(
 *     path="/api/orders",
 *     summary="Get your order",
 *     operationId="orders",
 *     tags={"Checkouts"},
 * @OA\RequestBody(
 *         required=true,
 *         description="Array of cart IDs",
 * @OA\MediaType(
 *             mediaType="application/json",
 * @OA\Schema(
 *                 type="object",
 *                 required={"cart_ids"},
 * @OA\Property(
 *                     property="cart_ids",
 *                     type="array",
 * @OA\Items(type="integer"),
 *                 ),
 *             ),
 *         ),
 *     ),
 * @OA\Response(
 *     response=422,
 *     description="The given data was invalid",
 * @OA\JsonContent(
 *         type="object",
 * @OA\Property(property="code",    type="integer", example=422),
 * @OA\Property(property="message", type="string", example="The given data was invalid"),
 * @OA\Property(
 *             property="errors",
 *             type="object",
 * @OA\Property(
 *                 property="cart_ids",
 *                 type="array",
 * @OA\Items(type="string",         example="appears id in the list is not owned")
 *             )
 *         )
 *     )
 * ),
 * @OA\Response(
 *         response=200,
 *         description="Successfully retrieved user orders",
 * @OA\JsonContent(
 *             type="object",
 * @OA\Property(property="success", type="boolean", example=true),
 * @OA\Property(property="message", type="string", example="successfully"),
 * @OA\Property(
 *        property="data", 
 *        type="array", 
 * @OA\Items(
 *            type="object", 
 * @OA\Property(
 *                property="orders", 
 *                type="array", 
 *                     @OA\Items(
 *                         type="object",
 *                         @OA\Property(property="quantity", type="integer", example=3),
 *                         @OA\Property(property="id", type="integer", example=24),
 *                         @OA\Property(property="nft_id", type="integer", example=20),
 *                         @OA\Property(
 *                             property="nfts",
 *                             type="object",
 *                             @OA\Property(property="id", type="integer", example=20),
 *                             @OA\Property(property="title", type="string", example="Art Est aperiam."),
 *                             @OA\Property(property="collection_id", type="integer", example=1),
 *                             @OA\Property(property="current_price", type="number", format="float", example=76.14)
 *                         )
 *                     ),

 *                ), 
 *            ), 
 *        ), 
 * 
 *    ),
 *         )
 *     )
 * )
 */


class GetOrder extends Controller
{
    public function __invoke(GetOrderRequest $request)
    {
        try{
            $cartIds = $request->input('cart_ids');
            $cartcc =Cart::with(
                        'nfts:id,title,collection_id'
                    )
                        ->select('cart.quantity', 'cart.id', 'cart.nft_id')->whereIn('id', $cartIds)->get()
                        ->map(
                            function ($item) {
                                if ($item->nfts) {
                                    $item->nfts->current_price=(float)NFT::find($item->nfts->id)->nftPrices->sortBy('created_at')->last()['price'];
                                }
                                return $item;
                            }
                        );
            return ResponseHelper::success(
                message:"successfully",
                data:$cartcc,
                statusCode:ResponseHelper::HTTP_OK
            );   
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }    
    }
}
