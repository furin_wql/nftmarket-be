<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * @OA\Get(
 *     path="/api/users/list",
 *     summary="Get 30 result of a User",
 *     operationId="getlistUser",
 *     description="Returns 30 user by page",
 *     tags={"User"},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="page number",
 *         required=false,
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Success",
 *     @OA\JsonContent(
 *         type="object",*    
*    @OA\Property(
*        property="success", 
*        type="boolean", 
*    ), 
*    @OA\Property(
*        property="message", 
*        type="string", 
*        example="Get list user success", 
*    ), 
*    @OA\Property(
*        property="data", 
*        type="object", 
*        @OA\Property(
*            property="users", 
*            type="object", 
*            @OA\Property(
*                property="current_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="data", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="name", 
*                        type="string", 
*                        example="Dr. Jolie Tromp", 
*                    ), 
*                    @OA\Property(
*                        property="avatar", 
*                        type="string", 
*                        example="https://via.placeholder.com/640x480.png/00eedd?text=excepturi", 
*                    ), 
*                    @OA\Property(
*                        property="email", 
*                        type="string", 
*                        example="beverly23@example.net", 
*                    ), 
*                    @OA\Property(
*                        property="gender", 
*                        type="string", 
*                        example="male", 
*                    ), 
*                    @OA\Property(
*                        property="phone_number", 
*                        type="string", 
*                        example="+14842675696", 
*                    ), 
*                    @OA\Property(
*                        property="custom_url", 
*                        type="string", 
*                        example="link/Keeley", 
*                    ), 
*                    @OA\Property(
*                        property="street_address", 
*                        type="string", 
*                        example="9717 Sanford Cape", 
*                    ), 
*                    @OA\Property(
*                        property="country_region", 
*                        type="string", 
*                        example="United States Minor Outlying Islands", 
*                    ), 
*                    @OA\Property(
*                        property="state", 
*                        type="string", 
*                        example="Rhode Island", 
*                    ), 
*                    @OA\Property(
*                        property="town_city", 
*                        type="string", 
*                        example="Orintown", 
*                    ), 
*                    @OA\Property(
*                        property="zip_code", 
*                        type="string", 
*                        example="109", 
*                    ), 
*                    @OA\Property(
*                        property="introduce_yourseft", 
*                        type="string", 
*                        example="Deserunt labore sed vel quia nesciunt.", 
*                    ), 
*                    @OA\Property(
*                        property="wallet_eth", 
*                        type="string", 
*                        example="2756.35", 
*                    ), 
*                    @OA\Property(
*                        property="confirm_google", 
*                        format="nullable", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="email_verified_at", 
*                        type="string", 
*                        example="2024-05-13T05:41:23.000000Z", 
*                    ), 
*                    @OA\Property(
*                        property="verifyToken", 
*                        format="nullable", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="created_at", 
*                        type="string", 
*                        example="2024-05-13T05:41:25.000000Z", 
*                    ), 
*                    @OA\Property(
*                        property="updated_at", 
*                        type="string", 
*                        example="2024-05-13T05:41:25.000000Z", 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="first_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/users/list?page=1", 
*            ), 
*            @OA\Property(
*                property="from", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/users/list?page=4", 
*            ), 
*            @OA\Property(
*                property="links", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="url", 
*                        format="nullable", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="label", 
*                        type="string", 
*                        example="&laquo; Previous", 
*                    ), 
*                    @OA\Property(
*                        property="active", 
*                        type="boolean", 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="next_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/users/list?page=2", 
*            ), 
*            @OA\Property(
*                property="path", 
*                type="string", 
*                example="http://localhost:1234/api/users/list", 
*            ), 
*            @OA\Property(
*                property="per_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="prev_page_url", 
*                format="nullable", 
*                type="string", 
*            ), 
*            @OA\Property(
*                property="to", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="total", 
*                type="number", 
*            ), 
*        ), 
*    ),
 *     ),
 * ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get nft"
 *     )
 * )
 */
class GetListUser extends Controller
{
    public function __invoke()
    {
        $user = User::paginate(30);
        try {
            return ResponseHelper::success(
                message: 'Get list user success',
                data :[
                    "users"=>$user
                ],
                statusCode:ResponseHelper::HTTP_OK
            );
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
