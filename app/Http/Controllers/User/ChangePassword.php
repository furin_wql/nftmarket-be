<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

/**
 * @OA\Put(
 *     path="/api/users/changepassword",
 *     summary="change password",
 *     operationId="changepassword",
 *     tags={"User"},
 *  @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="email",
 *                     type="string",
 *                     example="admin@gmail.com"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string",
 *                     example="12345"
 *                 ),
 *                 @OA\Property(
 *                     property="newpassword",
 *                     type="string",
 *                     example="123456"
 *                 ),
 *                 @OA\Property(
 *                     property="confirmpassword",
 *                     type="string",
 *                     example="123456"
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(
 *                  property="message", 
 *                  type="string", 
 *                  example="Change password success", 
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="
 *                      Wrong password
 *                      Confirm password does not match
 *                      Email does not match",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                  property="success", 
 *                  type="boolean", 
 *              ), 
 *             @OA\Property(
 *                  property="message", 
 *                  type="boolean",
 *                  example="Wrong password"
 *              ), 
 *         ),
 *     ),

 *     @OA\Response(
 *         response=422,
 *         description="The given data was invalid"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Error getting user"
 *     )
 * )
 */

class ChangePassword extends Controller
{
    public function __invoke(ChangePasswordRequest $request)
    {
        try {
            $input = $request->validated();
            $user = Auth::user();

            if ($input["email"] !== $user->email) {
                return ResponseHelper::error('Email does not match', ResponseHelper::HTTP_BAD_REQUEST);
            }

            if (!Hash::check($input["password"], $user->getAuthPassword())) {
                return ResponseHelper::error('Wrong password', ResponseHelper::HTTP_BAD_REQUEST);
            }

            if ($input['newpassword'] !== $input['confirmpassword']) {
                return ResponseHelper::error('Confirm password does not match', ResponseHelper::HTTP_BAD_REQUEST);
            }

            $user->password = bcrypt($input['newpassword']);
            $user->save();

            return ResponseHelper::success('Change password success');

        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
