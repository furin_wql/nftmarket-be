<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateAvatarRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Put(
 *     path="/api/users/avatar",
 *     summary="Update profile avatar",
 *     operationId="Updateprofileavatar",
 *     security={{"cookieAuth": {}}},
 *     description="update profile avatar<br/> Author: Huy",
 *     tags={"User"},
 *  @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="upload_files",
 *                     type="file",
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Update avatar success"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="The given data was invalid"
 *     ),   
 *     @OA\Response(
 *         response=500,
 *         description="Error getting user"
 *     )
 * )
 */
class UpdateProfileAvatar extends Controller
{
    public function __invoke(UpdateAvatarRequest $request)
    {
        try {
            $image = $request->file("upload_files");
            $user = Auth::user();

            if (Storage::disk("minio")->exists($user->avatar)) {
                Storage::disk("minio")->delete($user->avatar);
            }

            if ($image) {
                $user->avatar = Storage::disk("minio")->put("NFTFiles", $image);
                $user->save();
            }
            return ResponseHelper::success('Update avatar success');

        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
