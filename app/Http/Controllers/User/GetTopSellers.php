<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Get(
 *     path="/api/top-sellers",
 *     summary="Get top 8 sellers",
 *     operationId="getTopSellers",
 *     tags={"User"},
 *     description="This endpoint returns the top 8 sellers",
 *  @OA\Response(
 *     response=200,
 *     description="Success",
 *     @OA\JsonContent(
 *         type="object",
 *    @OA\Property(
 *        property="success",
 *        type="boolean",
 *    ),
 *    @OA\Property(
 *        property="message",
 *        type="string",
 *        example="get top sellers successfully",
 *    ),
 *    @OA\Property(
 *        property="data",
 *        type="array",
 *        @OA\Items(
 *            type="object",
 *            @OA\Property(
 *                property="name",
 *                type="string",
 *                example="Nicolas West",
 *            ),
 *            @OA\Property(
 *                property="avatar",
 *                type="string",
 *                example="https://via.placeholder.com/640x480.png/0055bb?text=vero",
 *            ),
 *            @OA\Property(
 *                property="email",
 *                type="string",
 *                example="nadia22@example.net",
 *            ),
 *            @OA\Property(
 *                property="total_sales",
 *                type="string",
 *                example="50",
 *               ),
 *            ),
 *       ),
 *   ),
 * ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get list Collections"
 *     )
 * )
 */

class GetTopSellers extends Controller
{
    public function __invoke()
    {
        try {
            $topSellers = User::select(
                "users.name",
                "users.avatar",
                "users.email",
                DB::raw(
                    "SUM(bill_nft.price_per_unit * bill_nft.quantity) as total_sales"
                )
            )
                ->join("nfts", "users.id", "=", "nfts.user_id")
                ->join("bill_nft", "nfts.id", "=", "bill_nft.nft_id")
                ->groupBy("users.id")
                ->orderBy("total_sales", "desc")
                ->limit(8)
                ->get();

            return ResponseHelper::success(
                message: "get top sellers successfully",
                data: ["users"=>$topSellers]
            );
        } catch (Exception $e) {
            return ResponseHelper::error(
                "An error occurred while get top sellers",
                500
            );
        }
    }
}
