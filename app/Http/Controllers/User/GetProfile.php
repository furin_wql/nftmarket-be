<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Get(
 *     path="/api/users",
 *     summary="Get profile",
 *     operationId="getProfile",
 *     security={{"cookieAuth": {}}},
 *     description="Get profile<br/> Author: Huy",
 *     tags={"User"},
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(
 *                 property="user",
 *                 type="object",
 *                 @OA\Property(property="id", type="integer", example=1),
 *                 @OA\Property(property="name", type="string", format="admin"),
 *                 @OA\Property(property="avatar", type="string", format="https://via.placeholder.com/640x480.png/001155?text=accusantium"),
 *                 @OA\Property(property="email", type="string", format="admin@gmail.com"),
 *                 @OA\Property(property="gender", type="string", example="female"),
 *                 @OA\Property(property="phone_number", type="string", example="+14193308274"),
 *                 @OA\Property(property="custom_url", type="string", example="854 Kaitlyn Estate Apt. 270"),
 *                 @OA\Property(property="country_region", type="string", example="Barbados"),
 *                 @OA\Property(property="state", type="string", example="New Hampshire"),
 *                 @OA\Property(property="town_city", type="string", example="East Wilmerstad"),
 *                 @OA\Property(property="zip_code", type="string", example="315"),
 *                 @OA\Property(property="introduce_yourseft", type="string", example="Odio ..."),
 *                 @OA\Property(property="wallet_eth", type="float", example="99999.99"),
 *                 @OA\Property(property="confirm_google", type="float", example="admin@gmail.com"),
 *                 @OA\Property(property="email_verified_at", type="string", format="date-time", example="2024-05-04T08:56:50.000000Z"),
 *                 @OA\Property(property="created_at", type="string", format="date-time", example="2024-05-04T08:56:50.000000Z"),
 *                 @OA\Property(property="updated_at", type="string", format="date-time", example="2024-04-28 06:43:27"),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Error getting user"
 *     )
 * )
 */

class GetProfile extends Controller
{
    public function __invoke()
    {
        try {
            return ResponseHelper::success(
                message: 'Get profile success',
                data :[
                    "user"=>Auth::user()
                ],
                statusCode:ResponseHelper::HTTP_OK
            );
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
