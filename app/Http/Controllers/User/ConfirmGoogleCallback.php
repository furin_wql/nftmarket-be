<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

/**
 * @OA\Get(
 *     path="/api/users/confirm/google/callback",
 *     summary="Confirm google",
 *     operationId="Confirmgoogle",
 *     tags={"User"},
 *     security={{"cookieAuth": {}}},
 *     description="
 *                  Google will return a link of the form 
 *                 https://web2-nft.d-soft.tech/api/users/confirm/google/callback?params
 *                  to the frontend and the frontend must transfer all those params to the backend according to 
 *                  https://be2-nft.d-soft.tech/api/users/confirm/google/callback?params
 *                  Author: Huy",
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(
 *                 property="message",
 *                 type="object",
 *                  example="Confirm google success",
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Server error"
 *     )
 * )
 */

class ConfirmGoogleCallback extends Controller
{
    public function __invoke()
    {
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
            $user = Auth::user();
            $user->confirm_google = $googleUser->email;
            $user->save();
            return ResponseHelper::success("Confirm google success");
        } catch (\Exception $exception) {
            return ResponseHelper::error($exception);
        }
    }
}
