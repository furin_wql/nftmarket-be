<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Put(
 *     path="/api/users",
 *     summary="Update profile",
 *     operationId="Updateprofile",
 *     security={{"cookieAuth": {}}},
 *     description="update profile<br/> Author: Huy",
 *     tags={"User"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="name",
 *                     type="string",
 *                     example="admin"
 *                 ),
 *                 @OA\Property(
 *                     property="gender",
 *                     type="string",
 *                     example="male"
 *                 ),
 *                 @OA\Property(
 *                     property="phone_number",
 *                     type="string",
 *                     example="+1234567899"
 *                 ),
 *                 @OA\Property(
 *                     property="custom_url",
 *                     type="string",
 *                     example="link/admin"
 *                 ),
 *                 @OA\Property(
 *                     property="street_address",
 *                     type="string",
 *                     example="452 Meggie Parkway"
 *                 ),
 *                 @OA\Property(
 *                     property="country_region",
 *                     type="string",
 *                     example="Bolivia"
 *                 ),
 *                 @OA\Property(
 *                     property="state",
 *                     type="string",
 *                     example="Louisiana"
 *                 ),
 *                 @OA\Property(
 *                     property="town_city",
 *                     type="string",
 *                     example="Saraichester"
 *                 ),
 *                 @OA\Property(
 *                     property="zip_code",
 *                     type="string",
 *                     example="999"
 *                 ),
 *                 @OA\Property(
 *                     property="introduce_yourseft",
 *                     type="string",
 *                     example="introduce_yourseft ..."
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Update profile success"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="The given data was invalid"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Error getting user"
 *     )
 * )
 */

class UpdateProfile extends Controller
{
    public function __invoke(UpdateProfileRequest $request)
    {
        try {
            $request = $request->validated();
            $user = Auth::user();
            $user->name =  $request['name'];
            $user->gender =  $request['gender'];
            $user->phone_number =  $request['phone_number'];
            $user->custom_url =  $request['custom_url'];
            $user->street_address =  $request['street_address'];
            $user->country_region =  $request['country_region'];
            $user->state =  $request['state'];
            $user->town_city =  $request['town_city'];
            $user->zip_code =  $request['zip_code'];
            $user->introduce_yourseft =  $request['introduce_yourseft'];
            $user->save();
            return ResponseHelper::success('Update profile success');
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
        
    }
}
