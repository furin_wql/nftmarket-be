<?php

namespace App\Http\Controllers\User;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

/**
 * @OA\Get(
 *     path="/api/users/confirm/google",
 *     summary="Get url confirm google",
 *     operationId="geturlconfirmgoogle",
 *     tags={"User"},
 *     security={{"cookieAuth": {}}},
 *     description="Get url confirm google <br/> Author: Huy",
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(
 *                 property="message",
 *                 type="object",
 *                  example="Get url confirm google success",
 *             ),
 *            @OA\Property(
            *        property="payload", 
            *        type="object", 
            *        @OA\Property(
            *            property="url", 
            *            type="string", 
            *            example="https://accounts.google.com/o/oauth2/auth?client_id=111738010464-5ejliogtl8o2l0l33slgb1qmv74t5gjr.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Fapi%2Fconfirm%2Fgoogle%2Fcallback&scope=openid+profile+email&response_type=code", 
            *        ), 
            *    ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Server error"
 *     )
 * )
 */

class GetURLConfirmGoogle extends Controller
{
    public function __invoke()
    {
        try {
            $url = Socialite::driver('google')->stateless()
                ->redirect()->getTargetUrl();
            return ResponseHelper::success(
                "Get url confirm google success",
                ['url'=>$url]
            );
        } catch (\Exception $exception) {
            return ResponseHelper::error($exception);
        }
    }
}
