<?php

namespace App\Http\Controllers\Contacts;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Jobs\SendEmail;
use App\Jobs\SendEmailContact;
use App\Models\Contact;
use Exception;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 *     path="/api/users/contact",
 *     summary="Send a contact message",
 *     operationId="sendContactMessage",
 *     tags={"Contact"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                  @OA\Property(property="name", type="string", description="Name of the user", example="John Doe"),
 *                  @OA\Property(property="email", type="string", format="email", description="Email address of the user", example="lathanhthu@gmail.com"),
 *                  @OA\Property(property="phone", type="string", description="Phone number of the user", example="123-456-7890"),
 *                  @OA\Property(property="category", type="string", description="Category of the contact", example="Support"),
 *                  @OA\Property(property="title", type="string", description="Title of the message", example="Issue with my account"),
 *                  @OA\Property(property="message", type="string", description="Message content", example="I am facing an issue with my account."),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Contact message sent successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="contact successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=429,
 *         description="You sent mail too many times",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="code", type="integer", example=429),
 *             @OA\Property(property="message", type="string", example="Too Many Attempts.")
 *         )
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Validation error",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="code", type="integer", example=422),
 *             @OA\Property(property="message", type="string", example="The given data was invalid"),
 *             @OA\Property(
 *                 property="errors",
 *                 type="object",
 *                 @OA\Property(
 *                     property="email",
 *                     type="array",
 *                     @OA\Items(type="string", example="The email field is required.")
 *                 )
 *             )
 *         )
 *     )
 * )
 */


class ContactMail extends Controller
{
    public function __invoke(ContactRequest $request)
    {
        try{    
            $data_contact = $request->validated();
            $data_contact['user_id'] = $request->user()->id;
            Contact::create($data_contact);
            SendEmailContact::dispatch($data_contact);
            return ResponseHelper::success(
                message:'successfully',
                statusCode:ResponseHelper::HTTP_OK,
            );
        }catch(Exception $e){
            return ResponseHelper::error(
                message: $e->getMessage(),
                statusCode:ResponseHelper::HTTP_INTERNAL_SERVER_ERROR,
            );
        }
    }
}
