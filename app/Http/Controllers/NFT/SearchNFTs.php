<?php

namespace App\Http\Controllers\NFT;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\NFT;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Get(
 *     path="/api/nfts",
 *     summary="Get 30 result of a NFT",
 *     operationId="getlistNFT",
 *     description="Get 30 nft detail<br/> Author: Huy",
 *     tags={"NFT"},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="page number",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="title",
 *         in="query",
 *         description="search title",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="price",
 *         in="query",
 *         description="value asc or desc",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="collection_id",
 *         in="query",
 *         description="collection id",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="user_id",
 *         in="query",
 *         description="user id",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="price_min",
 *         in="query",
 *         description="min price search",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="price_max",
 *         in="query",
 *         description="max price search",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="new_in",
 *         in="query",
 *         description="true or false, Within 1 month",
 *         required=false,
 *     ),
 *     @OA\Parameter(
 *         name="trending",
 *         in="query",
 *         description="true or false. Number of sales > 15 within the most recent month",
 *         required=false,
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Success",
 *     @OA\JsonContent(
 *         type="object",
 *          *    @OA\Property(
*        property="success", 
*        type="boolean", 
*    ), 
*    @OA\Property(
*        property="message", 
*        type="string", 
*        example="Get nfts success", 
*    ), 
*    @OA\Property(
*        property="data", 
*        type="object", 
*        @OA\Property(
*            property="nfts", 
*            type="object", 
*            @OA\Property(
*                property="current_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="data", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="created_at", 
*                        type="string", 
*                        example="2024-05-16T06:53:56.000000Z", 
*                    ), 
*                    @OA\Property(
*                        property="updated_at", 
*                        type="string", 
*                        example="2024-05-16T06:53:56.000000Z", 
*                    ), 
*                    @OA\Property(
*                        property="user_id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="collection_id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="title", 
*                        type="string", 
*                        example="Art Esse autem illo labore.", 
*                    ), 
*                    @OA\Property(
*                        property="starting_date", 
*                        type="string", 
*                        example="2024-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="expiration_date", 
*                        type="string", 
*                        example="2024-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="description", 
*                        type="string", 
*                        example="Beatae distinctio.", 
*                    ), 
*                    @OA\Property(
*                        property="img_url", 
*                        type="string", 
*                        example="https://via.placeholder.com/640x480.png/008877?text=odit", 
*                    ), 
*                    @OA\Property(
*                        property="current_price", 
*                        type="string", 
*                        example="8.94", 
*                    ), 
*                    @OA\Property(
*                        property="nft_prices", 
*                        type="array", 
*                        @OA\Items(
*                            type="object", 
*                            @OA\Property(
*                                property="id", 
*                                type="number", 
*                            ), 
*                            @OA\Property(
*                                property="created_at", 
*                                type="string", 
*                                example="2024-05-16T06:53:56.000000Z", 
*                            ), 
*                            @OA\Property(
*                                property="updated_at", 
*                                type="string", 
*                                example="2024-05-16T06:53:56.000000Z", 
*                            ), 
*                            @OA\Property(
*                                property="nft_id", 
*                                type="number", 
*                            ), 
*                            @OA\Property(
*                                property="price", 
*                                type="string", 
*                                example="8", 
*                            ), 
*                        ), 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="first_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/nfts?page=1", 
*            ), 
*            @OA\Property(
*                property="from", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/nfts?page=1", 
*            ), 
*            @OA\Property(
*                property="links", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="url", 
*                        format="nullable", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="label", 
*                        type="string", 
*                        example="&laquo; Previous", 
*                    ), 
*                    @OA\Property(
*                        property="active", 
*                        type="boolean", 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="next_page_url", 
*                format="nullable", 
*                type="string", 
*            ), 
*            @OA\Property(
*                property="path", 
*                type="string", 
*                example="http://localhost:1234/api/nfts", 
*            ), 
*            @OA\Property(
*                property="per_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="prev_page_url", 
*                format="nullable", 
*                type="string", 
*            ), 
*            @OA\Property(
*                property="to", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="total", 
*                type="number", 
*            ), 
*        ), 
*    ),
 *    ), 
 * ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get nft"
 *     )
 * )
 */

class SearchNFTs extends Controller
{
    public function __invoke(Request $request)
    {
        try {   
            $nfts= NFT::query();
            if ($request['trending']==true){
                $nfts=$nfts->select('nfts.*')
                    ->leftJoin('bill_nft', 'nfts.id', '=', 'bill_nft.nft_id')
                    ->where("bill_nft.created_at", '>=' , now()->subMonths(1))
                    ->groupBy('nfts.id')
                    ->havingRaw("SUM(bill_nft.quantity) >= 15");
            } 

            if ($request['title']){
                $nfts =  $nfts->where("title", 'ilike', '%'.$request['title'].'%');
            }   
            if ($request['collection_id']){
                $nfts =  $nfts->where("collection_id",$request['collection_id']);
            }  
            if ($request['user_id']){
                $nfts =  $nfts->where("user_id",$request['user_id']);
            }  
            if ($request['new_in']==true){
                $nfts =  $nfts->where("nfts.updated_at", '>=' , now()->subMonths(1));
            }  
            if ($request['price_min']){
                $nfts =  $nfts->where("current_price",'>=',$request['price_min']);
            }  
            if ($request['price_max']){
                $nfts =  $nfts->where("current_price",'<=',$request['price_max']);
            }
            if ($request['price']==='desc'){
                $nfts = $nfts->orderBy('current_price','desc');
            }
            if ($request['price']==='asc'){
                $nfts = $nfts->orderBy('current_price','asc');
            }

            $nfts = $nfts->with(['nftPrices'=> function ($q){
                            $q->where('created_at',">=",now()->subDays(7));
                            $q->orderBy('created_at','asc');
                        }]);

            $nfts=$nfts->paginate(30);
            return ResponseHelper::success('Get nfts success',["nfts"=>$nfts]);
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
