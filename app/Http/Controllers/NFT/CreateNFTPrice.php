<?php

namespace App\Http\Controllers\NFT;

use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\NFTPriceCreateRequest;
use App\Http\Requests\NFTUpdatePrice;
use App\Http\Requests\UpdatePriceNFTRequest;
use Exception;
use App\Models\NFT;
use App\Models\NFTPrice;

/**
 * @OA\Post(
 *     path="/api/nftprice",
 *     summary="Create price of a NFT",
 *     operationId="createNFTPrice",
 *     tags={"NFT"},
 *     @OA\RequestBody(
 *         required=true,
 *         description="Create price of a NFT",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="nft_id",
 *                 type="number",
 *                 description="Id of a NFT you selected",
 *                 example=2
 *             ),
 *             @OA\Property(
 *                 property="price",
 *                 type="number",
 *                 example=86.56
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=201,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="success"),
 *             @OA\Property(
 *                 property="data",
 *                 type="object",
 *                 @OA\Property(property="nft_id", type="integer", example=21),
 *                 @OA\Property(property="price", type="string", example="88.89"),
 *                 @OA\Property(property="updated_at", type="string", format="date-time"),
 *                 @OA\Property(property="created_at", type="string", format="date-time"),
 *                 @OA\Property(property="id", type="integer", example=44),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *          response=422,
 *          description="Unprocessable Entity",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(property="code", type="integer", example=422),
 *              @OA\Property(property="message", type="string", example="The given data was invalid"),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="nft_id",
 *                      type="array",
 *                      @OA\Items(type="string", example="The price field is required.")
 *                  ),
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Query Error",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="An error occurred while create nft price"
 *             ),
 *         ),
 *     ),
 * )
 */

class CreateNFTPrice extends Controller
{
    public function __invoke(NFTPriceCreateRequest $request)
    {
        try{
            $createrequest = $request->validated();
            NFTPrice::create($createrequest);
            $nfts = NFT::find($createrequest['nft_id']);
            $nfts->current_price = $createrequest['price'];
            $nfts->save();
            return ResponseHelper::success(
                "Create NFT Price successfully",
            );       
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
            );
        }
    }
}
