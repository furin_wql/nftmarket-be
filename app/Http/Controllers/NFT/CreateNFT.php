<?php

namespace App\Http\Controllers\NFT;

use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ValidateFileHelper;
use App\Http\Requests\NFTCreateRequest;
use App\Models\NFT;
use App\Models\NFTPrice;

/**
 * @OA\Post(
 *     path="/api/nfts",
 *     summary="Create a NFT",
 *     operationId="createNFT",
 *     tags={"NFT"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="title",
 *                     type="string",
 *                     description="Name of the NFT"
 *                 ),
 *                 @OA\Property(
 *                     property="collection_id",
 *                     type="number",
 *                     description="Id of the collection you selected"
 *                 ),
 *                 @OA\Property(
 *                     property="upload_files",
 *                     type="string",
 *                     format="binary",
 *                     description="file of the NFT"
 *                 ),
 *                 @OA\Property(
 *                     property="starting_date",
 *                     type="string",
 *                     format="date-time",
 *                     description="the start day"
 *                 ),
 *                 @OA\Property(
 *                     property="expiration_date",
 *                     type="string",
 *                     format="date-time",
 *                     description="the expiration day"
 *                 ),
 *                 @OA\Property(
 *                     property="nft_price",
 *                     type="number",
 *                     description="price of the NFT"
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=201,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="success"),
 *             @OA\Property(
 *                 property="nft",
 *                 type="object",
 *                 @OA\Property(property="id", type="integer", example=21),
 *                 @OA\Property(property="created_at", type="string", format="date-time"),
 *                 @OA\Property(property="updated_at", type="string", format="date-time"),
 *                 @OA\Property(property="user_id", type="integer", example=1),
 *                 @OA\Property(property="collection_id", type="integer", example=5),
 *                 @OA\Property(property="title", type="string", example="newnew"),
 *                 @OA\Property(property="starting_date", type="string", format="date-time", example="2024-04-28 06:43:27"),
 *                 @OA\Property(property="expiration_date", type="string", format="date-time", example="2024-04-28 06:43:27"),
 *                 @OA\Property(property="img_url", type="string", example="http://minio:9000/backend-nft-img/NFTFiles/vRQukaXYpjukjSAHUkkXCrMFi6bgzJCTxLeiSD1G.jpg"),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *          response=422,
 *          description="Unprocessable Entity",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(property="code", type="integer", example=422),
 *              @OA\Property(property="message", type="string", example="The given data was invalid"),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="nft_id",
 *                      type="array",
 *                      @OA\Items(type="string", example="The upload files field must be an image.")
 *                  ),
 *              ),
 *          ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Product not found"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Query error",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="An error occurred while creating nft."
 *             ),
 *         ),
 *     ),
 * )
 */

class CreateNFT extends Controller
{
    public function __invoke(NFTCreateRequest $request)
    {
        try {
            $nft = $request->validated();
            $nft["user_id"] = Auth::id();
            if ($request->file("upload_files")) {
                $image = $request->file("upload_files");
                $nft["img_url"] = Storage::disk("minio")->put("NFTFiles", $image);
            }
            $price = $nft["nft_price"];
            $nft['current_price'] = (float)$price;
            $nft = NFT::create($nft);
            NFTPrice::create([
                "nft_id" => $nft["id"],
                "price" => $price,
            ]);
            return ResponseHelper::success(
                message:"Create NFT successfully",
                statusCode:ResponseHelper::HTTP_CREATED
            );       
        } catch (Exception $e) {
            return ResponseHelper::error($e->getMessage());
        }
    }
}
