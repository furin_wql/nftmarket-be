<?php

namespace App\Http\Controllers\NFT;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartRequest;
use App\Models\Cart;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Post(
 *     path="/api/carts",
 *     summary="Add NFT to Cart",
 *     operationId="addNFTtoCart",
 *     tags={"NFT"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="nft_id",
 *                     type="integer",
 *                     description="Id of the NFT"
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=201,
 *         description="NFT added to the cart successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="success"),
 *             @OA\Property(
 *                 property="data",
 *                 type="object",
 *                 @OA\Property(property="nft_id", type="integer", example=20),
 *                 @OA\Property(property="user_id", type="integer", example=5),
 *                 @OA\Property(property="quantity", type="integer", example=1),
 *                 @OA\Property(property="updated_at", type="string", format="date-time"),
 *                 @OA\Property(property="created_at", type="string", format="date-time"),
 *                 @OA\Property(property="id", type="integer", example=29),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="NFT added to the cart successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="success"),
 *             @OA\Property(
 *                 property="data",
 *                 type="object",
 *                 @OA\Property(property="nft_id", type="integer", example=20),
 *                 @OA\Property(property="user_id", type="integer", example=5),
 *                 @OA\Property(property="quantity", type="integer", example=2),
 *                 @OA\Property(property="updated_at", type="string", format="date-time"),
 *                 @OA\Property(property="created_at", type="string", format="date-time"),
 *                 @OA\Property(property="id", type="integer", example=29),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Query error",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="An error occurred while add nft to cart"
 *             ),
 *         ),
 *     ),
 *       @OA\Response(
 *          response=422,
 *          description="Unprocessable Entity",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(property="code", type="integer", example=422),
 *              @OA\Property(property="message", type="string", example="The given data was invalid"),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="nft_id",
 *                      type="array",
 *                      @OA\Items(type="string", example="The nft id field must be a number.")
 *                  ),
 *              ),
 *          ),
 *      )
 * )
 */


class AddNFTtoCart extends Controller
{
    public function __invoke(CartRequest $request)
    {
        try{
            $nft = $request->validated();
            $nft['user_id']= Auth::id();

            $cartItems = Cart::where('user_id',$nft['user_id'])->where('nft_id',$nft['nft_id'])->first();
            if($cartItems){
                $cartItems->quantity+=1;
                $cartItems->save();
                return ResponseHelper::success(
                    message:"Add items to cart successfully",
                    statusCode:ResponseHelper::HTTP_OK
                );
            }
            $nft['quantity']= 1;
            $cart = Cart::create($nft);
            return ResponseHelper::success(
                message:"Add items to cart successfully",
                statusCode:ResponseHelper::HTTP_OK
            );       
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }
       
    }
}
