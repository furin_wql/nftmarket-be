<?php

namespace App\Http\Controllers\NFT;

use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\NFTGetDetail;
use Exception;
use App\Models\NFT;

/**
 * @OA\Get(
 *     path="/api/nfts/{id}",
 *     summary="Get details of a NFT",
 *     operationId="Id NFT",
 *     tags={"NFT"},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         required=true,
 *         description="ID of the NFT",
 *         @OA\Schema(
 *             type="integer",
 *             format="int64"
 *         )
 *     ),
 * @OA\Response(
 *     response=200,
 *     description="Success",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(
 *             property="message",
 *             type="string",
 *             example="success"
 *         ),
 *         @OA\Property(
 *             property="data",
 *             type="object",
 *             @OA\Property(
 *                 property="id",
 *                 type="integer",
 *                 example=21
 *             ),
 *             @OA\Property(
 *                 property="created_at",
 *                 type="string",
 *                 format="date-time",
 *                 example="2024-05-02T09:42:25.000000Z"
 *             ),
 *             @OA\Property(
 *                 property="updated_at",
 *                 type="string",
 *                 format="date-time",
 *                 example="2024-05-02T09:42:25.000000Z"
 *             ),
 *             @OA\Property(
 *                 property="user_id",
 *                 type="integer",
 *                 example=1
 *             ),
 *             @OA\Property(
 *                 property="collection_id",
 *                 type="integer",
 *                 example=5
 *             ),
 *             @OA\Property(
 *                 property="title",
 *                 type="string",
 *                 example="newnew"
 *             ),
 *             @OA\Property(
 *                 property="starting_date",
 *                 type="string",
 *                 format="date-time",
 *                 example="2024-04-28 06:43:27"
 *             ),
 *             @OA\Property(
 *                 property="expiration_date",
 *                 type="string",
 *                 format="date-time",
 *                 example="2024-04-28 06:43:27"
 *             ),
 *             @OA\Property(
 *                 property="description",
 *                 type="string",
 *                 example="abc",
 *             ),
 *             @OA\Property(
 *                 property="img_url",
 *                 type="string",
 *                 format="uri",
 *                 example="http://minio:9000/backend-nft-img/NFTFiles/vRQukaXYpjukjSAHUkkXCrMFi6bgzJCTxLeiSD1G.jpg"
 *             ),
 *             @OA\Property(
 *                 property="nft_prices",
 *                 type="array",
 *                 @OA\Items(
 *                     type="object",
 *                     @OA\Property(
 *                         property="id",
 *                         type="integer",
 *                         example=41
 *                     ),
 *                     @OA\Property(
 *                         property="created_at",
 *                         type="string",
 *                         format="date-time",
 *                         example="2024-05-02T09:42:25.000000Z"
 *                     ),
 *                     @OA\Property(
 *                         property="updated_at",
 *                         type="string",
 *                         format="date-time",
 *                         example="2024-05-02T09:42:25.000000Z"
 *                     ),
 *                     @OA\Property(
 *                         property="nft_id",
 *                         type="integer",
 *                         example=21
 *                     ),
 *                     @OA\Property(
 *                         property="price",
 *                         type="number",
 *                         format="float",
 *                         example=90.89
 *                     ),
 *                 ),
 *             ),
 *         ),
 *     ),
 * ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get nft"
 *     )
 * )
 */

class GetNFTDetail extends Controller
{
    public function __invoke(NFT $nft_id)
    {
        try{
            $nft_id = $nft_id->id;
            $nft_details = NFT::with(['nftPrices'=> function ($q){
                                    $q->where('created_at',">=",now()->subDays(7));
                                    $q->orderBy('created_at','asc');
                                }])
                                ->findOrFail($nft_id);
            return ResponseHelper::success(
                "get item in cart successfully",
                ['nfts'=>$nft_details]
            );       
        }catch(Exception $e){
            return ResponseHelper::error($e->getMessage());
        }
    }
}
