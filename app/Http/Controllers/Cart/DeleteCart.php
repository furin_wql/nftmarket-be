<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteCartRequest;
use App\Models\Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Delete(
 *     path="/api/carts/{cart_id}",
 *     summary="Delete NFT from Cart",
 *     operationId="DeleteCart",
 *     tags={"Cart"},
 *     @OA\Parameter(
 *         name="cart_id",
 *         in="path",
 *         required=true,
 *         description="ID of the NFT in Cart",
 *         @OA\Schema(
 *             type="integer",
 *             format="int64"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="OK",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="success"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Item not found",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="message", type="string", example="Item not found"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get list item in cart",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="error", type="string", example="error..."),
 *         ),
 *     ),
 * )
 */


class DeleteCart extends Controller
{
    public function __invoke(Request $request, $cart_id)
    {
        try{
            $user_id= Auth::id();
            $cartItems = Cart::where('id',$cart_id)->where('user_id', $user_id)->first();
            if(!$cartItems){
                return ResponseHelper::error(
                    'Item not found',
                    ResponseHelper::HTTP_NOT_FOUND
                );
            }
            $cartItems->delete();
            return ResponseHelper::success(
                message:"Delete items from cart successfully",
                statusCode:ResponseHelper::HTTP_CREATED
            );
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
