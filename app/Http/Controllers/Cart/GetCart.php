<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\NFT;
use App\Notifications\InvoicePaid;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Get(
 *     path="/api/carts",
 *     summary="Get list item in shopping cart",
 *     operationId="getListIteminCart",
 *     tags={"Cart"},
 *     @OA\Response(
 *         response=200,
 *         description="Get item in cart successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="get item in cart successfully"),
 *             @OA\Property(
 *                 property="data",
 *                 type="array",
 *                 @OA\Items(
 *                     type="object",
 *                     @OA\Property(property="quantity", type="integer", example=1),
 *                     @OA\Property(property="id", type="integer", example=29),
 *                     @OA\Property(property="nft_id", type="integer", example=18),
 *                     @OA\Property(
 *                         property="nfts",
 *                         type="object",
 *                         @OA\Property(property="id", type="integer", example=18),
 *                         @OA\Property(property="title", type="string", example="Art Sint molestias vero."),
 *                         @OA\Property(property="collection_id", type="integer", example=3),
 *                         @OA\Property(property="current_price", type="number", format="float", example=97.6)
 *                     )
 *                 )
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while get list item in cart",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="error", type="string", example="error..."),
 *         ),
 *     ),
 * )
 */


class GetCart extends Controller
{
    public function __invoke()
    {
        try{
            $listcart = Cart::with(
                'nfts:id,title,collection_id,current_price'
            )
                ->select('cart.quantity', 'cart.id', 'cart.nft_id')->where('user_id',Auth::id())->get();
            return ResponseHelper::success(
                "get item in cart successfully",
                $listcart,
                ResponseHelper::HTTP_OK
            );       
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        
    }
}
