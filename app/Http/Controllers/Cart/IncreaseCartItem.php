<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Put(
 *     path="/api/carts/{cart_id}/increase",
 *     summary="Increase quantity of an item in the shopping cart",
 *     operationId="increaseCartItem",
 *     tags={"Cart"},
 *     @OA\Parameter(
 *         name="cart_id",
 *         in="path",
 *         required=true,
 *         description="ID of the shopping cart",
 *         @OA\Schema(type="integer", format="int64")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Increase successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Item not found in the shopping cart",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Item not found in your shopping cart")
 *         )
 *     )
 * )
 */


class IncreaseCartItem extends Controller
{
    public function __invoke(Request $request, $cart_id)
    {
        try{
            $cartItem = Cart::where('id',$cart_id)
                ->where('user_id',$request->user()->id)
                ->first();
            if(!$cartItem){
                return ResponseHelper::error('Item not found in your shopping cart',
                    ResponseHelper::HTTP_NOT_FOUND
                );
            }
            $cartItem->increment('quantity');
            return ResponseHelper::success(
                message:"Increase successfully",
                statusCode:ResponseHelper::HTTP_OK
            );       
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
