<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Put(
 *     path="/api/carts/{cart_id}/decrease",
 *     summary="Decrease quantity of an item in the shopping cart",
 *     operationId="decreaseCartItem",
 *     tags={"Cart"},
 *     @OA\Parameter(
 *         name="cart_id",
 *         in="path",
 *         required=true,
 *         description="ID of the shopping cart",
 *         @OA\Schema(type="integer", format="int64")
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Decrease successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Item not found in the shopping cart",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Item not found in your shopping cart")
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Cannot decrease further. Minimum quantity reached",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Cannot decrease further. Minimum quantity reached")
 *         )
 *     )
 * )
 */

class DecreaseCartItem extends Controller
{
    public function __invoke(Request $request,$cart_id)
    {
        try{
            $cartItem = Cart::where('id',$cart_id)
                ->where('user_id',$request->user()->id)
                ->first();
            if(!$cartItem){
                return ResponseHelper::error('Not found item',
                    ResponseHelper::HTTP_NOT_FOUND
                );
            }
            $minQuantity = 1;
            if($cartItem->quantity <= $minQuantity){
                return ResponseHelper::error('Cannot decrease further. Minimum quantity reached',
                    ResponseHelper::HTTP_BAD_REQUEST
                );
            }
            $cartItem->decrement('quantity');
            return ResponseHelper::success(
                message:"Decrease successfully",
                statusCode:ResponseHelper::HTTP_OK
            );       
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }
}
