<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

/**
 * @OA\Post(
 *     path="/api/forgot-password",
 *     summary="Send password reset link to email",
 *     description="Khi người dùng nhập email vào form quên mật khẩu, hệ thống gửi link đến email, link này trả về giao diện reset password kèm theo token và user id.",
 *     operationId="forgotPassword",
 *     tags={"Auth"},
*     @OA\RequestBody(
 *         required=true,
 *         description="User email address",
 *         @OA\JsonContent(
 *             type="object",
 *             required={"email"},
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 format="email",
 *                 example="user1@gmail.com"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Password reset link sent successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Password reset link sent successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=429,
 *         description="Too many password reset attempts",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Too many password reset attempts. Please try again later")
 *         )
 *     ),
 *      @OA\Response(
 *         response=404,
 *         description="Invalid user",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Invalid user")
 *         )
 *     ),
 *      @OA\Response(
 *         response=400,
 *         description="Failed to send password reset link",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Failed to send password reset link")
 *         )
 *     ),
 * )
 */

class ForgotPasword extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate(["email" => "required|email"]);

        $status = $this->sendResetLinkEmail($request);

        switch ($status) {
            case Password::RESET_LINK_SENT:
                return ResponseHelper::success(
                    message: "Password reset link sent successfully",
                    statusCode: ResponseHelper::HTTP_OK
                );

            case Password::RESET_THROTTLED:
                return ResponseHelper::error(
                    message: "Too many password reset attempts. Please try again later.",
                    statusCode: ResponseHelper::HTTP_TOO_MANY_REQUESTS
                );

            case Password::INVALID_USER:
                return ResponseHelper::error(
                    message: "Invalid user.",
                    statusCode: ResponseHelper::HTTP_NOT_FOUND
                );

            default:
                return ResponseHelper::error(
                    message: "Failed to send password reset link.",
                    statusCode: ResponseHelper::HTTP_BAD_REQUEST
                );
        }
    }

    private function sendResetLinkEmail(Request $request)
    {
        // customize the Password::sendResetLink method
        $status = Password::broker()->sendResetLink(
            $request->only("email"),
            function ($user, $token) {
                $resetTokenUrl = url(
                    "http://localhost:3000/reset-password/{$user->id}/{$token}"
                );
                SendEmail::dispatch($user, $resetTokenUrl);
            }
        );

        return $status;
    }
}
