<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

/**
 * @OA\Get(
 *     path="/api/email/verify/{id}/{hash}",
 *     summary="Verify email address",
 *     operationId="verifyEmail",
 *     tags={"Auth"},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         required=true,
 *         description="User ID",
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *             example=12
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hash",
 *         in="path",
 *         required=true,
 *         description="Verification hash",
 *         @OA\Schema(
 *             type="string",
 *             example="DuCZphzFmPWcE0zBNrFElASb15hW2KTU5m5XDSqkyCbYgSHOGxtAPJKpCTqY"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Authentication successful",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="successful authentication")
 *         )
 *     ),
 * )
 */

class VerifyEmail extends Controller
{
    public function __invoke($userId, $token)
    {
        try{
            $user = User::where('id',$userId)->first();
            if($user->verifyToken == $token){
                $user->markEmailAsVerified();
                return redirect()->to(config('app.front_end_url')."/verify?verify=true");
            }
            else{
                return redirect()->to(config('app.front_end_url')."/verify?verify=false");
            }
        }catch(Exception $e){
            return redirect()->to(config('app.front_end_url')."/verify?verify=false");
        }
       
    }
}
