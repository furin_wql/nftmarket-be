<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\AuthenticationException;
use App\Helpers\ResponseHelper;
use App\Helpers\SessionHelper;
use Carbon\Carbon;
use App\Models\SessionModel;

/**
 * @OA\Post(
 *     path="/api/login",
 *     summary="login",
 *     operationId="createProduct",
 *     tags={"Auth"},
 *     @OA\RequestBody(
 *         required=true,
 *         description="login data",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="email",
 *                 type="string",
 *                 example="mocthanh.furin@gmail.com"
 *             ),
 *             @OA\Property(
 *                 property="password",
 *                 type="string",
 *                 example="12345678"
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=201,
 *         description="Login success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Login success"),
 *             @OA\Property(
*                   property="data", 
*                   type="object", 
*                   @OA\Property(
*                       property="user", 
*                       type="object", 
*                       @OA\Property(
*                           property="id", 
*                           type="number", 
*                       ), 
*               @OA\Property(
*                property="name", 
*                type="string", 
*                example="Burnice Fay Jr.", 
*            ), 
*            @OA\Property(
*                property="avatar", 
*                type="string", 
*                example="https://via.placeholder.com/640x480.png/00eeff?text=quia", 
*            ), 
*            @OA\Property(
*                property="email", 
*                type="string", 
*                example="btromp@example.org", 
*            ), 
*            @OA\Property(
*                property="gender", 
*                type="string", 
*                example="male", 
*            ), 
*            @OA\Property(
*                property="phone_number", 
*                type="string", 
*                example="+15857717027", 
*            ), 
*            @OA\Property(
*                property="custom_url", 
*                type="string", 
*                example="link/Patrick", 
*            ), 
*            @OA\Property(
*                property="street_address", 
*                type="string", 
*                example="50716 Hoppe Knoll Apt. 406", 
*            ), 
*            @OA\Property(
*                property="country_region", 
*                type="string", 
*                example="Lebanon", 
*            ), 
*            @OA\Property(
*                property="state", 
*                type="string", 
*                example="Kansas", 
*            ), 
*            @OA\Property(
*                property="town_city", 
*                type="string", 
*                example="Kleinhaven", 
*            ), 
*            @OA\Property(
*                property="zip_code", 
*                type="string", 
*                example="262", 
*            ), 
*            @OA\Property(
*                property="introduce_yourseft", 
*                type="string", 
*                example="Est quia doloribus dolore.", 
*            ), 
*            @OA\Property(
*                property="wallet_eth", 
*                type="string", 
*                example="511.45", 
*            ), 
*            @OA\Property(
*                property="confirm_google", 
*                format="nullable", 
*                type="string", 
*                example="null",
*            ), 
*            @OA\Property(
*                property="email_verified_at", 
*                type="string", 
*                example="2024-05-20T16:22:54.000000Z", 
*            ), 
*            @OA\Property(
*                property="created_at", 
*                type="string", 
*                example="2024-05-20T16:22:57.000000Z", 
*            ), 
*            @OA\Property(
*                property="updated_at", 
*                type="string", 
*                example="2024-05-20T16:22:57.000000Z", 
*            ), 
 *                  ),
 *            ),
 *        ),
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="User is already logged in",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                 property="message",
 *                 type="string",
 *                 example="User is already logged in"
 *             ),
 *         ),
 *     ),
 * )
 */

class LoginUser extends Controller
{
    public function __invoke(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        if (Auth::attempt($credentials)) {
            if (!Auth::user()->hasVerifiedEmail()){
                Auth::logout();
                return ResponseHelper::error('Your email address is not verified',
                ResponseHelper::HTTP_UNAUTHORIZED);
            }
            $user_id = Auth::id();
            $exitstingSession = SessionModel::where('user_id', $user_id)->first();
            $resp= SessionHelper::sessionExpired($exitstingSession);
            return $resp;
        } else {
            throw new  AuthenticationException('Account incorrect');
        }   
    }
}
