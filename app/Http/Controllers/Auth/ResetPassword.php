<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;


/**
 * @OA\Post(
 *     path="/api/reset-password",
 *     summary="Reset password",
 *     operationId="resetPassword",
 *     security={{"cookieAuth": {}}},
 *     description="Reset password <br/> Author: Huy",
 *     tags={"Auth"},
 * @OA\RequestBody(
 *     required=true,
 *     description="Reset password data",
 *     @OA\JsonContent(
 *         type="object",
 *         required={"token", "email", "password", "password_confirmation"},
 *         @OA\Property(
 *             property="token",
 *             description="Token",
 *             type="string",
 *             example="b1a2c3d4e5f6g7h8i9j0"
 *         ),
 *         @OA\Property(
 *             property="email",
 *             description="User's email",
 *             type="string",
 *             format="email",
 *             example="user@gmail.com"
 *         ),
 *         @OA\Property(
 *             property="password",
 *             description="New password",
 *             type="string",
 *             format="password",
 *             example="password123"
 *         ),
 *         @OA\Property(
 *             property="password_confirmation",
 *             description="New password confirmation",
 *             type="string",
 *             format="password",
 *             example="password123"
 *         )
 *     )
 * ),
 *     @OA\Response(
 *         response=200,
 *         description="Password reset successfully",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="Password reset successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=400,
 *         description="Invalid token",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Invalid token")
 *         )
 *     ),
 *      @OA\Response(
 *         response=404,
 *         description="Invalid user",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="Invalid user")
 *         )
 *     ),
 *      @OA\Response(
 *         response=500,
 *         description="failed to reset password",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=false),
 *             @OA\Property(property="message", type="string", example="failed to reset password")
 *         )
 *     ),
 * )
 */

class ResetPassword extends Controller
{
    public function __invoke(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);
     
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (User $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
     
                $user->save();
     
                // event(new PasswordReset($user));
            }
        );
        switch ($status) {
            case Password::PASSWORD_RESET:
                return ResponseHelper::success(
                    message: "Password reset successfully.",
                    statusCode: ResponseHelper::HTTP_OK
                );
        
            case Password::INVALID_TOKEN:
                return ResponseHelper::error(
                    message: "Invalid token.",
                    statusCode: ResponseHelper::HTTP_BAD_REQUEST
                );
        
            case Password::INVALID_USER:
                return ResponseHelper::error(
                    message: "Invalid user.",
                    statusCode: ResponseHelper::HTTP_NOT_FOUND
                );
        
            default:
                return ResponseHelper::error(
                    message: "failed to reset password.",
                    statusCode: ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
                );
        }
    }
}
