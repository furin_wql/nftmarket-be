<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignupRequest;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyEmail;
use App\Models\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

 
 /**
 * @OA\Post(
 *     path="/api/signup",
 *     summary="Signup",
 *     operationId="Signup",
 *     tags={"Auth"},
 *     @OA\RequestBody(
 *         required=true,
 *         description="Signup",
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="email",
 *                     type="string",
 *                     description="email",
 *                     example="yasashiirin@gmail"
 *                 ),
 *                 @OA\Property(
 *                     property="password",
 *                     type="string",
 *                     description="password",
 *                     example="12345678",
 *                 ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response=201,
 *          description="Registered successfully and sent email verification",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(property="success", type="boolean", example=true),
 *              @OA\Property(property="message", type="string", example="Registered successfully and sent email verification"),
 *          ),
 *     ),
 *      @OA\Response(
 *          response=422,
 *          description="The given data was invalid",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(property="code", type="integer", example=422),
 *              @OA\Property(property="message", type="string", example="The given data was invalid"),
 *              @OA\Property(
 *                  property="errors",
 *                  type="object",
 *                  @OA\Property(
 *                      property="email",
 *                      type="array",
 *                      @OA\Items(type="string", example="The email has already been taken.")
 *                  ),
 *              ),
 *          ),
 *      )
 * )
 */


class Signup extends Controller
{
    public function __invoke(SignupRequest $request)
    {
        try{
            $input = $request->validated();
            $input['password'] = bcrypt($input['password']);
            $input['name']= $input['email'];

            $token=  Str::random(60);
            $input['verifyToken'] = $token;

            $user = User::create($input);
            $user_id = $user->id;
            
            // $verificationUrl = url("/api/email/verify/{$user_id}/{$token}");
            $verificationUrl = config('app.url')."/api/email/verify/{$user_id}/{$token}";
            SendEmail::dispatch($user, $verificationUrl);
            return ResponseHelper::success(
                message:"Registered successfully and sent email verification",
                statusCode:ResponseHelper::HTTP_CREATED
            );
        }catch(Exception $e){
            return ResponseHelper::error(
                $e->getMessage(),
                ResponseHelper::HTTP_INTERNAL_SERVER_ERROR
            );
        }

    }
}
