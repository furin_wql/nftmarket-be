<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SessionModel;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Session;

  /**
     * @OA\Get(
     *     path="/api/logout",
     *     summary="Logout a user",
     *     operationId="logout",
     *     tags={"Auth"},
     *     @OA\Response(
     *         response=404,
     *         description="account_incorrect",
     *     )
     * )
     */
class LogoutUser extends Controller
{
    public function __invoke()
    {
        
        if (Auth::check()) {
            $user_id = Auth::id();
            SessionModel::where('user_id', $user_id)->delete();
            Auth::logout();
            return ResponseHelper::success('Logout success');
        }
    }
}
