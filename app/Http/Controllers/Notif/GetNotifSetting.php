<?php

namespace App\Http\Controllers\Notif;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\NotifySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Get(
 *     path="/api/users/notify-setting",
 *     summary="Get notify setting",
 *     operationId="getnotifysetting",
 *     tags={"User"},
 *     security={{"cookieAuth": {}}},
 *     description="Get notify setting detail <br/> Author: Huy",
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(
 *                 property="message",
 *                 type="object",
 *                  example="Get notify setting success",
 *             ),
 *             *    @OA\Property(
*        property="data", 
*        type="object", 
*        @OA\Property(
*            property="notifysetting", 
*            type="array", 
*            @OA\Items(
*                type="object", 
*                @OA\Property(
*                    property="id", 
*                    type="number",
*                    example="1",
*                ), 
*                @OA\Property(
*                    property="created_at", 
*                    type="string", 
*                    example="2024-05-13T05:41:25.000000Z", 
*                ), 
*                @OA\Property(
*                    property="updated_at", 
*                    type="string", 
*                    example="2024-05-13T05:41:25.000000Z", 
*                ), 
*                @OA\Property(
*                    property="name", 
*                    type="string", 
*                    example="Et V", 
*                ), 
*                @OA\Property(
*                    property="notify_setting_details_id", 
*                    format="nullable", 
*                    type="string", 
*                ), 
*                @OA\Property(
*                    property="value", 
*                    type="boolean",
*                ), 
*            ), 
*        ), 
*    ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Error getting user notify setting"
 *     )
 * )
 */

class GetNotifSetting extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $notifysetting = NotifySetting::select(
                'notify_settings.*',
                'notify_setting_details.id as notify_setting_details_id',
                'notify_setting_details.value as value'
                )
            ->leftJoin('notify_setting_details', function($join)
            {
                $join->on('notify_settings.id', '=', 'notify_setting_details.notify_setting_id');
                $join->where('notify_setting_details.user_id', '=', Auth::id());
            })
            ->get();
            foreach ($notifysetting as $item) {
                if (is_null($item->value)){
                    $item->value = false;
                }
            }
            return ResponseHelper::success('Get notify setting success',["notifysetting"=>$notifysetting]);
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }

    }

}
