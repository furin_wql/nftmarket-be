<?php

namespace App\Http\Controllers\Notif;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingNotifRequest;
use App\Models\NotifySettingDetail;
use AWS\CRT\HTTP\Response;
use Exception;
use Illuminate\Http\Request;
use LDAP\Result;

/**
 * @OA\Post(
 *     path="/api/users/notify-setting",
 *     summary="Toggle notification setting",
 *     operationId="toggleNotificationSetting",
 *     security={{"cookieAuth": {}}},
 *     tags={"Notification"},
 *     @OA\RequestBody(
 *         required=true,
 *         description="ID of the setting and enabled status",
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                  required={"setting_id", "enabled"},
 *                  @OA\Property(property="setting_id", type="integer", example=1),
 *                  @OA\Property(property="enabled", type="number", example=0)

 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Successfully toggled notification setting",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="successfully")
 *         )
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="The given data was invalid",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="code", type="integer", example=422),
 *             @OA\Property(property="message", type="string", example="The given data was invalid"),
 *             @OA\Property(
 *                 property="errors",
 *                 type="object",
 *                 @OA\Property(
 *                     property="enabled",
 *                     type="array",
 *                     @OA\Items(type="string", example="The enabled field must be true or false.")
 *                 )
 *             )
 *         )
 *     )
 * )
 */


class ToggleSetting extends Controller
{
    public function __invoke(SettingNotifRequest $request)
    {
        try{
            $setting_id = $request->input('setting_id');
            $enabled = $request->input('enabled');
            
            NotifySettingDetail::updateOrCreate(
                ['user_id' => $request->user()->id , 'notify_setting_id' => $setting_id],
                ['user_id' => $request->user()->id ,'notify_setting_id' => $setting_id, 'value' => $enabled]
            );
            return ResponseHelper::success(
                message:'successfully',
                statusCode:ResponseHelper::HTTP_OK,
            );
        }catch(Exception $e){
            return ResponseHelper::error(
                message:$e->getMessage(),
                statusCode:ResponseHelper::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
