<?php

namespace App\Http\Controllers\Favorite;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddFavouriteNFTRequest;
use App\Models\FavouriteNFT;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 *     path="/api/favorite/nfts",
 *     summary="add favorite nfts",
 *     operationId="addfavoritenfts",
 *     security={{"cookieAuth": {}}},
 *     description="add favorite nfts",
 *     tags={"Favorite"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="nft_id",
 *                     type="string",
 *                     example="1"
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(property="message", type="string", example="Add nft to favorite list success"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *                @OA\Property(
            *        property="code", 
            *        type="number", 
            *        example="422", 
            *    ), 
            *    @OA\Property(
            *        property="message", 
            *        type="string", 
            *        example="The given data was invalid", 
            *    ), 
            *    @OA\Property(
            *        property="errors", 
            *        type="object", 
            *        @OA\Property(
            *            property="nft_id", 
            *            type="array", 
            *            @OA\Items(
            *                type="string", 
            *                example="The selected nft id is invalid.", 
            *            ), 
            *        ), 
            *    ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Server error"
 *     )
 * )
 */


class AddFavoriteNFT extends Controller
{
    public function __invoke(AddFavouriteNFTRequest $request){
        try{
            $fv = $request->validated();
            $fv['user_id']= $request->user()->id;

            $favouriteDb = FavouriteNFT::where("user_id", $fv["user_id"])->where("nft_id", $fv["nft_id"])->first();
            if($favouriteDb){
                return ResponseHelper::error(message:"This nft already exists in favorites",
                statusCode:ResponseHelper::HTTP_BAD_REQUEST);
            }
            FavouriteNFT::create($fv);
            return ResponseHelper::success(
                message:"Add NFT to favorite list success",
                statusCode: ResponseHelper::HTTP_CREATED,
            );
        }catch(\Exception $e){
            return ResponseHelper::error($e->getMessage());
        }

    }
}
