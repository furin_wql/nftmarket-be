<?php

namespace App\Http\Controllers\Favorite;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Post(
 *     path="/api/favorite/collections",
 *     summary="add favorite collections",
 *     operationId="addfavoritecollections",
 *     security={{"cookieAuth": {}}},
 *     description="add favorite collections<br/> Author: Huy",
 *     tags={"Favorite"},
 *     @OA\RequestBody(
 *         required=true,
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 @OA\Property(
 *                     property="collection_id",
 *                     type="string",
 *                     example="1"
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(property="message", type="string", example="Add collection to favorite list success"),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
 *                @OA\Property(
            *        property="code", 
            *        type="number", 
            *        example="422", 
            *    ), 
            *    @OA\Property(
            *        property="message", 
            *        type="string", 
            *        example="The given data was invalid", 
            *    ), 
            *    @OA\Property(
            *        property="errors", 
            *        type="object", 
            *        @OA\Property(
            *            property="collection_id", 
            *            type="array", 
            *            @OA\Items(
            *                type="string", 
            *                example="The selected collection id is invalid.", 
            *            ), 
            *        ), 
            *    ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Server error"
 *     )
 * )
 */

class AddFavoriteCollection extends Controller
{
    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            'collection_id' => 'required|exists:collections,id',
        ]);
        try {
            $exists = DB::table('favorite_collection')
            ->where('collection_id',$validated['collection_id'])
            ->where('user_id',Auth::id())->first();
            if ($exists) {
                return ResponseHelper::success("Collection is already in the favorite list");
            }
            DB::table('favorite_collection')->insert([
                'collection_id' => $validated['collection_id'],
                'user_id' => Auth::id(),
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            return ResponseHelper::success("Add collection to favorite list success");
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
