<?php

namespace App\Http\Controllers\Favorite;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @OA\Get(
 *     path="/api/favorite/collections",
 *     summary="get favorite collections",
 *     operationId="getfavoritecollections",
 *     security={{"cookieAuth": {}}},
 *     description="get favorite collections<br/> Author: Huy",
 *     tags={"Favorite"},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="page number",
 *         required=false,
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Success",
 *         @OA\JsonContent(
 *             type="object",
*    @OA\Property(
*        property="success", 
*        type="boolean", 
*    ), 
*    @OA\Property(
*        property="message", 
*        type="string", 
*        example="Get collections favorite success", 
*    ), 
*    @OA\Property(
*        property="payload", 
*        type="object", 
*        @OA\Property(
*            property="collections", 
*            type="object", 
*            @OA\Property(
*                property="current_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="data", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="created_at", 
*                        type="string", 
*                        example="2024-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="updated_at", 
*                        type="string", 
*                        example="2024-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="user_id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="category_id", 
*                        type="number", 
*                    ), 
*                    @OA\Property(
*                        property="price", 
*                        type="string", 
*                        example="56.96", 
*                    ), 
*                    @OA\Property(
*                        property="name", 
*                        type="string", 
*                        example="Collection Maiores unde.", 
*                    ), 
*                    @OA\Property(
*                        property="url", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="starting_date", 
*                        type="string", 
*                        example="2024-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="expiration_date", 
*                        type="string", 
*                        example="2025-05-16 13:53:56", 
*                    ), 
*                    @OA\Property(
*                        property="description", 
*                        type="string", 
*                        example="Aut dolores qui.", 
*                    ), 
*                    @OA\Property(
*                        property="is_explicit_and_sensitive", 
*                        type="boolean", 
*                    ), 
*                    @OA\Property(
*                        property="logo_img_url", 
*                        type="string", 
*                        example="https://via.placeholder.com/640x480.png/0022ee?text=unde", 
*                    ), 
*                    @OA\Property(
*                        property="feature_img_url", 
*                        type="string", 
*                        example="https://via.placeholder.com/640x480.png/009999?text=et", 
*                    ), 
*                    @OA\Property(
*                        property="cover_img_url", 
*                        type="string", 
*                        example="https://via.placeholder.com/640x480.png/009988?text=et", 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="first_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/favorite/collections?page=1", 
*            ), 
*            @OA\Property(
*                property="from", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="last_page_url", 
*                type="string", 
*                example="http://localhost:1234/api/favorite/collections?page=1", 
*            ), 
*            @OA\Property(
*                property="links", 
*                type="array", 
*                @OA\Items(
*                    type="object", 
*                    @OA\Property(
*                        property="url", 
*                        format="nullable", 
*                        type="string", 
*                    ), 
*                    @OA\Property(
*                        property="label", 
*                        type="string", 
*                        example="&laquo; Previous", 
*                    ), 
*                    @OA\Property(
*                        property="active", 
*                        type="boolean", 
*                    ), 
*                ), 
*            ), 
*            @OA\Property(
*                property="next_page_url", 
*                format="nullable", 
*                type="string", 
*            ), 
*            @OA\Property(
*                property="path", 
*                type="string", 
*                example="http://localhost:1234/api/favorite/collections", 
*            ), 
*            @OA\Property(
*                property="per_page", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="prev_page_url", 
*                format="nullable", 
*                type="string", 
*            ), 
*            @OA\Property(
*                property="to", 
*                type="number", 
*            ), 
*            @OA\Property(
*                property="total", 
*                type="number", 
*            ), 
*        ), 
*    ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Unauthenticated"
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="Server error"
 *     )
 * )
 */

class GetFavoriteCollection extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            $collections = DB::table('favorite_collection')
            ->select('c.*')
            ->where('favorite_collection.user_id',Auth::id())
            ->join('collections as c','favorite_collection.collection_id','=','c.id')->paginate(30);
            return ResponseHelper::success("Get collections favorite success",['collections'=>$collections]);
        } catch (\Throwable $th) {
            return ResponseHelper::error($th);
        }
    }
}
