<?php

namespace App\Http\Controllers\Favorite;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\FavouriteNFT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Get(
 *     path="/api/favorite/nfts",
 *     summary="Get list favourite of NFTs",
 *     operationId="getNFTs",
 *     tags={"Favorite"},
 *     @OA\Response(
 *         response=200,
 *         description="Successfully retrieved list of NFTs",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="success", type="boolean", example=true),
 *             @OA\Property(property="message", type="string", example="successfully"),
 *             @OA\Property(
 *                 property="payload",
 *                 type="array",
 *                 @OA\Items(
 *                     type="object",
 *                     @OA\Property(property="id", type="integer", example=8),
 *                     @OA\Property(property="title", type="string", example="Art Iusto ipsam fugit laboriosam."),
 *                     @OA\Property(property="user_id", type="integer", example=10),
 *                     @OA\Property(property="description", type="string", example="Ratione et eaque."),
 *                     @OA\Property(property="current_price", type="number", nullable=true, example=null),
 *                     @OA\Property(property="collection_id", type="integer", example=3),
 *                     @OA\Property(property="starting_date", type="string", format="date-time", example="2024-05-23 11:44:39"),
 *                     @OA\Property(property="expiration_date", type="string", format="date-time", example="2024-05-23 11:44:39"),
 *                     @OA\Property(property="img_url", type="string", example="https://via.placeholder.com/640x480.png/003366?text=id"),
 *                 ),
 *             ),
 *         ),
 *     ),
 *     @OA\Response(
 *         response=500,
 *         description="An error occurred while retrieving the list of NFTs",
 *         @OA\JsonContent(
 *             type="object",
 *             @OA\Property(property="error", type="string", example="An error occurred while retrieving the list of NFTs"),
 *         ),
 *     ),
 * )
 */


class GetFavoriteNFT extends Controller
{
    public function __invoke(){
        try{
            $user_id = Auth::user()->id;
            $listfv = FavouriteNFT::join('nfts','favourite_nft.nft_id','=','nfts.id')
            ->where('favourite_nft.user_id', '=', $user_id,)
            ->select('nfts.id','nfts.title','nfts.user_id','nfts.description','nfts.current_price',
                    'nfts.collection_id','nfts.starting_date','nfts.expiration_date','nfts.expiration_date','nfts.img_url')
            ->get();
            return ResponseHelper::success(
                message:"successfully",
                data:["nfts" => $listfv],
                statusCode: ResponseHelper::HTTP_OK
            );
        }catch(\Exception $e){
            return ResponseHelper::error($e->getMessage());
        }
    }
}
