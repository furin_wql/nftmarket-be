<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Helpers\ResponseHelper;
use Carbon\Carbon;

class SessionHelper
{
    public static function sessionExpired($exitstingSession)
    {
        if ($exitstingSession) {
            $expirationTime = config('session.lifetime') * 60;
            $lastActivity = Carbon::parse($exitstingSession->last_activity);
            $timePass = Carbon::now()->diffInSeconds($lastActivity);
            if ($timePass > $expirationTime) {
                return ResponseHelper::success(
                    message:"Login success",
                    data: ["user"=>Auth::user()],
                    statusCode:ResponseHelper::HTTP_OK
                );
            } else {
                Auth::logout();
                return ResponseHelper::error('Account is logged in somewhere else',ResponseHelper::HTTP_UNAUTHORIZED);
            }
        } else {
            return ResponseHelper::success(
                message:"Login success",
                data: ["user"=>Auth::user()],
                statusCode:ResponseHelper::HTTP_OK
            );
        }
    }
}