<?php

namespace App\Jobs;

use App\Mail\ContactEmail;
use App\Mail\SendMailFeedback;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailContact implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $data_contact;

    /**
     * Create a new job instance.
     */
    public function __construct($data_contact)
    {
        $this->data_contact = $data_contact;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        Mail::mailer('smtp')->to(config('app.admin_gmail'))->send( new ContactEmail($this->data_contact));
        Mail::mailer('smtp')->to($this->data_contact['email'])->send( new SendMailFeedback($this->data_contact));
    }
}
